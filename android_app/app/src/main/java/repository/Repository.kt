package repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import dataclasses.*
import mqtt.MQTTService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofitapi.RetrofitSingleton

object Repository {
    // SUBCRIPTION TOPIC AND MQTT SERVICE - CLIENT
    lateinit var mqttService : MQTTService
    lateinit var topicList : ArrayList<String>
    // TOKEN FOR AUTHENTICATION AND AUTHORIZATION
    private lateinit var token : String
    fun getUserToken() = token
    fun setUserToken(accessToken : String){
        token = accessToken
    }
    fun getWithToken(token: String){
        val call = RetrofitSingleton.retrofitUserAPI.testGetWithToken(token)
        call.enqueue(object : Callback<String> {
            override fun onResponse(
                call: Call<String>,
                response: Response<String>
            ) {
                if(response.code() == 200){
                    Log.d("TEST_GET_USER_WITH_TOKEN:", response.body().toString())
                }

            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.d("TEST_GET_USER_WITH_TOKEN:", "TRY GET WITH TOKEN FAILED: ${t.message}")
            }

        })
    }
    // USER MODEL:
    private lateinit var currentUser: User
    fun getCurrentUser() = currentUser
    fun setCurrentUser(user : User) {
        currentUser = user
    }
//    fun loginUser(user: User){
//        val call = RetrofitSingleton.retrofitUserAPI.authenticateUser(user)
//        call.enqueue(object : Callback<UserRelatedResponse> {
//            override fun onResponse(
//                call: Call<UserRelatedResponse>,
//                response: Response<UserRelatedResponse>
//            ) {
//                if(response.code() == 200 && response.body()!!.message == AUTHENTICATE_ACCOUNT_SUCCESS_MESSAGE){
//                    currentUser = user
//                }
//
//            }
//
//            override fun onFailure(call: Call<UserRelatedResponse>, t: Throwable) {
//
//            }
//
//        })
//    }
    // DOOR MODEL:
    private val doorLiveData :MutableLiveData<ArrayList<Door>> = MutableLiveData<ArrayList<Door>>()

    fun getDoorLivedata() : MutableLiveData<ArrayList<Door>>{
        val call = RetrofitSingleton.retrofitDoorAPI.getAllDoorState()

        call.enqueue(object : Callback<ArrayList<Door>> {
            override fun onResponse(call: Call<ArrayList<Door>>, response: Response<ArrayList<Door>>) {
                if(!response.isSuccessful){
                    Log.d("GET_DOORS:","Response went fine, But not SUCESSFUL with Code: ${response.code()}- ThinhDepTrai")
                }
                doorLiveData.value = response.body()
            }

            override fun onFailure(call: Call<ArrayList<Door>>, t: Throwable) {
                Log.d("GET_DOORS_FAILED:","${t.message}")
            }
        })
        return doorLiveData
    }
    fun updateDoorStateWithMQTT(doorID: String, door: Door): MutableLiveData<ArrayList<Door>>{
        Log.d(
            "CHANGE_DOOR_REPO_MQTT:",
            "DOOR RETURNED: ${door.id} with state ${door.state}"
        )
        val listDoor = doorLiveData.value
        if (door != null) {
            Log.d(
                "CHANGE_DOOR_REPO_MQTT:",
                "DOOR RETURNED: ${door.id} with state ${door.state}"
            )
        }
        if (door != null) {
            listDoor?.set(listDoor.indexOf(listDoor.find {
                it.id == door.id
            }), door)
        }
        doorLiveData.value = listDoor ?: doorLiveData.value
        return doorLiveData
    }
    fun changeDoorState(doorID : String, newDoor: Door): MutableLiveData<ArrayList<Door>> {
        val call =
            RetrofitSingleton.retrofitDoorAPI.changeDoorStateWithID(id = doorID, door = newDoor)
        call.enqueue(object : Callback<Door> {
            override fun onResponse(call: Call<Door>, response: Response<Door>) {
//                if (!response.isSuccessful) {
//                    Log.d(
//                        "CHANGE_DOOR_REPO:",
//                        "Response went fine, But not SUCESSFUL with Code: ${response.code()}- ThinhDepTrai"
//                    )
//                }
//                val returnDoor = response.body()
//                val listDoor = doorLiveData.value
//                if (returnDoor != null) {
//                    Log.d(
//                        "CHANGE_DOOR_REPO:",
//                        "DOOR RETURNED: ${returnDoor.id} with state ${returnDoor.state}"
//                    )
//                }
//                if (returnDoor != null) {
//                    listDoor?.set(listDoor.indexOf(listDoor.find {
//                        it.id == returnDoor.id
//                    }), returnDoor)
//                }
//                doorLiveData.value = listDoor ?: doorLiveData.value
            }

            override fun onFailure(call: Call<Door>, t: Throwable) {
                Log.d("GET_DOORS_FAILED:", "${t.message}")
            }
        })
        return doorLiveData
    }

    // command param: CLOSE_ALL / OPEN_ALL
    fun commandAllDoor(command : String): MutableLiveData<ArrayList<Door>> {
        val batchCommand = DoorBatchCommand(command)
        val call = RetrofitSingleton.retrofitDoorAPI.changeAllDoorState(batchCommand)

        call.enqueue(object: Callback<ArrayList<Door>>{
            override fun onResponse(
                call: Call<ArrayList<Door>>,
                response: Response<ArrayList<Door>>
            ) {
                if(!response.isSuccessful){
                    Log.d(
                        "BATCH_DOOR_REPO:",
                        "Response went fine, But not SUCESSFUL with Code: ${response.code()}- ThinhDepTrai"
                    )
                }
                doorLiveData.value = response.body()
            }

            override fun onFailure(call: Call<ArrayList<Door>>, t: Throwable) {
                Log.d("GET_DOORS_FAILED:","${t.message}")
            }

        })
        return doorLiveData
    }

    // INTRUSION MODEL:
    private val intrusionLiveData : MutableLiveData<ArrayList<Intrusion>> = MutableLiveData<ArrayList<Intrusion>>()
    fun getIntrusionLiveData() : MutableLiveData<ArrayList<Intrusion>>{
        val call = RetrofitSingleton.retrofitIntrusionAPI.getAllDoorIntrusion(token)

        call.enqueue(object : Callback<ArrayList<Intrusion>>{
            override fun onResponse(
                call: Call<ArrayList<Intrusion>>,
                response: Response<ArrayList<Intrusion>>
            ) {
                if(!response.isSuccessful){
                    Log.d("GET INTRUSION:","FAILED WITH CODE: ${response.code()} and body ${response.body()}")
                }
                intrusionLiveData.value = response.body()
            }

            override fun onFailure(call: Call<ArrayList<Intrusion>>, t: Throwable) {
                Log.d("GET INTRUSION:","FAILED WITH EXCEPTION: ${t.message}}")
            }
        })
        return intrusionLiveData
    }

    fun getSpecificDoorIntrusion(id : String) : MutableLiveData<ArrayList<Intrusion>>{
        val call = RetrofitSingleton.retrofitIntrusionAPI.getDoorIntrusionWithID(token,id)

        call.enqueue(object : Callback<ArrayList<Intrusion>>{
            override fun onResponse(
                call: Call<ArrayList<Intrusion>>,
                response: Response<ArrayList<Intrusion>>
            ) {

                if(!response.isSuccessful){
                    Log.d("GET INTRUSION:","FAILED WITH CODE: ${response.code()}")
                }
                val Intrusion = response.body()
//                Log.d("REPO INTRUSION ID $id: ","SUCCESSFUL WITH: Door ${Intrusion!![0].doorID} at time:${Intrusion[0].time} ")

                intrusionLiveData.value = response.body()
            }

            override fun onFailure(call: Call<ArrayList<Intrusion>>, t: Throwable) {
                Log.d("GET INTRUSION ID $id: ","FAILED WITH EXCEPTION: ${t.message}}")
            }

        })
        return intrusionLiveData
    }

    fun getDoorIntrusionWithIDinInterval(id : String, from: String, to: String) : MutableLiveData<ArrayList<Intrusion>>{
        val call = RetrofitSingleton.retrofitIntrusionAPI.getDoorIntrusionWithIDinInterval(token,id,from,to)

        call.enqueue(object : Callback<ArrayList<Intrusion>>{
            override fun onResponse(
                call: Call<ArrayList<Intrusion>>,
                response: Response<ArrayList<Intrusion>>
            ) {
                if(!response.isSuccessful){
                    Log.d("GET INTRUSION:","FAILED WITH CODE: ${response.code()}")
                }
                intrusionLiveData.value = response.body()
            }

            override fun onFailure(call: Call<ArrayList<Intrusion>>, t: Throwable) {
                Log.d("GET INTRUSION ID $id: ","FAILED WITH EXCEPTION: ${t.message} IN INTERVAL: FROM $from to $to}")
            }

        })
        return intrusionLiveData
    }



    // STATISTIC MODEL:
    private val statLiveData : MutableLiveData<ArrayList<Stat>> = MutableLiveData<ArrayList<Stat>>()
    fun getStatLiveData() : MutableLiveData<ArrayList<Stat>>{
        val call = RetrofitSingleton.retrofitStatAPI.getAllEntranceStat()

        call.enqueue(object : Callback<ArrayList<Stat>> {
            override fun onResponse(call: Call<ArrayList<Stat>>, response: Response<ArrayList<Stat>>){
                if (!response.isSuccessful){
                    Log.d("GET_STAT:", "FAILED with code: ${response.code()} - Thin-NhiNhanh")
                }
                statLiveData.value = response.body()
            }
            override fun onFailure(call: Call<ArrayList<Stat>>, t: Throwable){
                Log.d("GET_STAT_FAILED: ", "${t.message}")
            }
        })
        return statLiveData
    }


    private val notiLiveData : MutableLiveData<ArrayList<Notification>> = MutableLiveData<ArrayList<Notification>>()

    fun getNotiLiveData() : MutableLiveData<ArrayList<Notification>>{
        val call = RetrofitSingleton.retrofitNotificationAPI.getNotification()

        call.enqueue(object : Callback<ArrayList<Notification>> {
            override fun onResponse(
                call: Call<ArrayList<Notification>>,
                response: Response<ArrayList<Notification>>
            ) {
                if (!response.isSuccessful){
                    Log.d("GET_NOTI:", "Failed with code ${response.code()} - Thin Nguyen-Duy")
                }
                notiLiveData.value = response.body()
            }

            override fun onFailure(call: Call<ArrayList<Notification>>, t: Throwable) {
                Log.d("GET_NOTI_FAILED: ", "${t.message}")
            }
        })
        return notiLiveData
    }

    // SECURITY
//    private var securityLiveData : String? = null
//    fun getSecurityLiveData(): String? {
//        val call = RetrofitSingleton.retrofitSecurityAPI.getSecurityLevel()
//
//        call.enqueue(object: Callback<String>{
//            override fun onResponse(call: Call<String>, response: Response<String>) {
//                if (!response.isSuccessful){
//                    Log.d("GET_SECURITY_LEVEL:", "FAILED with code: ${response.code()} - NPTHONG")
//                }
//                securityLiveData = response.body()!!
//            }
//
//            override fun onFailure(call: Call<String>, t: Throwable) {
//                Log.d("GET_SECURITY_LEVEL_FAILED: ", "${t.message}")
//            }
//
//        })
//        return securityLiveData
//    }

//    fun changeDoorState(doorID : String, newState: String): MutableLiveData<ArrayList<Door>>{
//        val call = RetrofitSingleton.retrofitDoorAPI.changeDoorStateWithID(id= doorID, value = newState)
//        call.enqueue(object: Callback<Door>{
//            override fun onResponse(call: Call<Door>, response: Response<Door>) {
//                if(!response.isSuccessful){
//                    Log.d("CHANGE_DOOR_REPO:","Response went fine, But not SUCESSFUL with Code: ${response.code()}- ThinhDepTrai")
//                }
//                doorLiveData.value
//            }
//
//            override fun onFailure(call: Call<Door>, t: Throwable) {
//            }
//
//        })
//
//    }

}
