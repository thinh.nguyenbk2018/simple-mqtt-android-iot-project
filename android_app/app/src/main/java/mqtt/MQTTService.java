package mqtt;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

import repository.Repository;

public class MQTTService{
    // possible usernames: npthong/duythinbmt/thanhngodinh
    final String serverUri = "tcp://io.adafruit.com:1883";
    final String clientId = Repository.INSTANCE.getUserToken();
    String subscriptionTopic = "thinhnguyenbk2018/feeds/thinh-test-feed";
    ArrayList<String> subscriptionTopicList = new ArrayList<String>();
    String username = "thinhnguyenbk2018";
    String password = "aio_nspC93izeul2rJG7hFdVqRYmucJN";

    public MqttAndroidClient mqttAndroidClient;
    // MQTT Service with multiple topic to subscribe to
    public MQTTService(Context context, ArrayList<String> topicList){
        this.subscriptionTopicList = topicList;
        mqttAndroidClient = new MqttAndroidClient(context,serverUri,clientId);
        mqttAndroidClient.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {
                Log.w("mqtt",serverURI);
            }

            @Override
            public void connectionLost(Throwable cause) {

            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                Log.w("Mqtt",message.toString());
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });
        connect(true);
    }

    public MQTTService(Context context){
        mqttAndroidClient = new MqttAndroidClient(context,serverUri,clientId);
        mqttAndroidClient.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {
                Log.w("mqtt",serverURI);
            }

            @Override
            public void connectionLost(Throwable cause) {

            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                Log.w("Mqtt",message.toString());
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });
        connect(false);
    }
    public void setCallBack(MqttCallbackExtended callBack){
        mqttAndroidClient.setCallback(callBack);
    }

    private void connect(final Boolean multipleTopic){
//        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
//        mqttConnectOptions.setCleanSession(true);
        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setAutomaticReconnect(true);
        mqttConnectOptions.setCleanSession(false);
        mqttConnectOptions.setUserName(username);
        mqttConnectOptions.setPassword(password.toCharArray());
        try{
            mqttAndroidClient.connect(mqttConnectOptions, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Log.w("MQTT_SERVICE","SUCCESSFULLY CONNECTED");
                    DisconnectedBufferOptions  disconnectedBufferOptions= new DisconnectedBufferOptions();
                    disconnectedBufferOptions.setBufferEnabled(true);
                    disconnectedBufferOptions.setBufferSize(100);
                    disconnectedBufferOptions.setPersistBuffer(false);
                    disconnectedBufferOptions.setDeleteOldestMessages(false);
                    mqttAndroidClient.setBufferOpts(disconnectedBufferOptions);
                    if(multipleTopic){
                        subscribeToTopicList(subscriptionTopicList);
                    }
                    else{
                        subscribeToTopic();
                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.w("MQTT_SERVICE"," Failed to connect to: " + serverUri + exception.toString()
                            + "\n Topic is: " + Arrays.toString(asyncActionToken.getTopics())
                            +"\n Client:" +asyncActionToken.getClient().getClientId()
                            + "\n info: " + asyncActionToken.getClient().getServerURI());
                    exception.printStackTrace();
                }
            });
        }
        catch (MqttException ex){
            ex.printStackTrace();
        }
    }
    private void subscribeToTopic(){
        try{
            mqttAndroidClient.subscribe(subscriptionTopic, 0, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Log.w("Mqtt","Subscribed!");
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.w("Mqtt","Subscibedfail!");
                }
            });
        }
        catch (MqttException ex){
            System.err.println("Exceptionsubscribing");
            ex.printStackTrace();
        }
    }

    private void subscribeToTopicList(ArrayList<String> topics){
        String[] topicList = new String[topics.size()];
        topicList = topics.toArray(topicList);
        int[] qosList = new int[topics.size()];
        try{
            mqttAndroidClient.subscribe(topicList, qosList, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Log.w("Mqtt","Subscribed!");
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.w("Mqtt","Subscibedfail!");
                }
            });
        }
        catch (MqttException ex){
            System.err.println("Exceptionsubscribing");
            ex.printStackTrace();
        }
    }
}

//public class MQTTService{
//    // possible usernames: npthong/duythinbmt/thanhngodinh
//    final String serverUri = "tcp://io.adafruit.com:1883";
//    final String clientId = Repository.INSTANCE.getUserToken();
////    final String clientId = "thisismykeyhehehe";
//    String subscriptionTopic = "thinhnguyenbk2018/feeds/thinh-test-feed";
//
//
//
//    ArrayList<String> subscriptionTopicList = new ArrayList<String>();
//    String username = "thanhngodinh";
//    String password;
//
//    // ngodinhthanh broker passwotd:aio_yVxV32RrR0Wgq2h3cDp3txUmyWTh
//    public MqttAndroidClient mqttAndroidClient;
//    // MQTT Service with multiple topic to subscribe to
//    public MQTTService(Context context, ArrayList<String> topicList) throws PackageManager.NameNotFoundException {
//        ApplicationInfo appInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
//        Bundle metaBundle = appInfo.metaData;
//        String key = metaBundle.get("adaFruitKey").toString();
//        this.password = key;
//        Log.d("INIT_SERVICE:","API KEY IS: " +  key);
//        this.subscriptionTopicList = topicList;
//
//        mqttAndroidClient = new MqttAndroidClient(context,serverUri,clientId);
//        mqttAndroidClient.setCallback(new MqttCallbackExtended() {
//            @Override
//            public void connectComplete(boolean reconnect, String serverURI) {
//                Log.w("mqtt",serverURI);
//            }
//
//            @Override
//            public void connectionLost(Throwable cause) {
//
//            }
//
//            @Override
//            public void messageArrived(String topic, MqttMessage message) throws Exception {
//                Log.w("Mqtt","callbackInsideClass: " + message.toString());
//            }
//
//            @Override
//            public void deliveryComplete(IMqttDeliveryToken token) {
//
//            }
//        });
//        connect(true);
//    }
//
//    public MQTTService(Context context){
//        mqttAndroidClient = new MqttAndroidClient(context,serverUri,clientId);
//        mqttAndroidClient.setCallback(new MqttCallbackExtended() {
//            @Override
//            public void connectComplete(boolean reconnect, String serverURI) {
//                Log.w("mqtt",serverURI);
//            }
//
//            @Override
//            public void connectionLost(Throwable cause) {
//
//            }
//
//            @Override
//            public void messageArrived(String topic, MqttMessage message) throws Exception {
//                Log.w("Mqtt",message.toString());
//            }
//
//            @Override
//            public void deliveryComplete(IMqttDeliveryToken token) {
//
//            }
//        });
//        connect(false);
//    }
//    public void setCallBack(MqttCallbackExtended callBack){
//        mqttAndroidClient.setCallback(callBack);
//    }
//
//    private void connect(final Boolean multipleTopic){
////        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
////        mqttConnectOptions.setCleanSession(true);
//        Log.d("PASSWORD_CONNECTED:", password);
//        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
//        mqttConnectOptions.setAutomaticReconnect(true);
//        mqttConnectOptions.setCleanSession(false);
//        mqttConnectOptions.setUserName(username);
//        mqttConnectOptions.setPassword(password.toCharArray());
//        try{
//            mqttAndroidClient.connect(mqttConnectOptions, null, new IMqttActionListener() {
//                @Override
//                public void onSuccess(IMqttToken asyncActionToken) {
//                    Log.w("MQTT_SERVICE","SUCCESSFULLY CONNECTED");
//                    DisconnectedBufferOptions  disconnectedBufferOptions= new DisconnectedBufferOptions();
//                    disconnectedBufferOptions.setBufferEnabled(true);
//                    disconnectedBufferOptions.setBufferSize(100);
//                    disconnectedBufferOptions.setPersistBuffer(false);
//                    disconnectedBufferOptions.setDeleteOldestMessages(false);
//                    mqttAndroidClient.setBufferOpts(disconnectedBufferOptions);
//                    if(multipleTopic){
//                        subscribeToTopicList(subscriptionTopicList);
//                    }
//                    else{
//                        subscribeToTopic();
//                    }
//                }
//
//                @Override
//                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
//                    Log.w("MQTT_SERVICE"," Failed to connect to: " + serverUri + exception.toString()
//                            + "\n Topic is: " + Arrays.toString(asyncActionToken.getTopics())
//                            +"\n Client:" +asyncActionToken.getClient().getClientId()
//                            + "\n info: " + asyncActionToken.getClient().getServerURI());
//                    exception.printStackTrace();
//                }
//            });
//        }
//        catch (MqttException ex){
//            ex.printStackTrace();
//        }
//    }
//    private void subscribeToTopic(){
//        try{
//            mqttAndroidClient.subscribe(subscriptionTopic, 0, null, new IMqttActionListener() {
//                @Override
//                public void onSuccess(IMqttToken asyncActionToken) {
//                    Log.w("Mqtt","Subscribed!");
//                }
//
//                @Override
//                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
//                    Log.w("Mqtt","Subscibedfail!");
//                }
//            });
//        }
//        catch (MqttException ex){
//            System.err.println("Exceptionsubscribing");
//            ex.printStackTrace();
//        }
//    }
//
//    private void subscribeToTopicList(ArrayList<String> topics){
//        String[] topicList = new String[topics.size()];
//        topicList = topics.toArray(topicList);
//        int[] qosList = new int[topics.size()];
//        try{
//            mqttAndroidClient.subscribe(topicList, qosList, null, new IMqttActionListener() {
//                @Override
//                public void onSuccess(IMqttToken asyncActionToken) {
//                    Log.w("Mqtt","Subscribed!");
//                }
//
//                @Override
//                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
//                    Log.w("Mqtt","Subscibedfail!");
//                }
//            });
//        }
//        catch (MqttException ex){
//            System.err.println("Exceptionsubscribing");
//            ex.printStackTrace();
//        }
//    }
//}
