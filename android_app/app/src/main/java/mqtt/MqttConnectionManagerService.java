//package mqtt;
//
//
//import android.app.Service;
//import android.content.Intent;
//import android.os.IBinder;
//import android.util.Log;
//
//import androidx.annotation.Nullable;
//
//import org.eclipse.paho.android.service.MqttAndroidClient;
//import org.eclipse.paho.client.mqttv3.IMqttActionListener;
//import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
//import org.eclipse.paho.client.mqttv3.IMqttToken;
//import org.eclipse.paho.client.mqttv3.MqttCallback;
//import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
//import org.eclipse.paho.client.mqttv3.MqttException;
//import org.eclipse.paho.client.mqttv3.MqttMessage;
//
//import java.util.ArrayList;
//
//import repository.Repository;
//
//public class MqttConnectionManagerService extends Service {
//    // possible usernames: npthong/duythinbmt/thanhngodinh
//    final String serverUri = "tcp://io.adafruit.com:1883";
//    final String clientId = Repository.INSTANCE.getUserToken();
//    ArrayList<String> subscriptionTopicList = new ArrayList<String>();
//    String username = "thinhnguyenbk2018";
//    String password = "aio_nspC93izeul2rJG7hFdVqRYmucJN";
//    private MqttAndroidClient client;
//    private MqttConnectOptions options;
//
//    @Override
//    public void onCreate() {
//        super.onCreate();
//        options = createMqttConnectOptions();
//        client = createMqttAndroidClient();
//    }
//
//
//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        this.connect(client, options);
//        return START_STICKY;
//    }
//
//    @Nullable
//    @Override
//    public IBinder onBind(Intent intent) {
//        return null;
//    }
//
//    private MqttConnectOptions createMqttConnectOptions() {
//        //create and return options
//        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
//        mqttConnectOptions.setAutomaticReconnect(true);
//        mqttConnectOptions.setCleanSession(false);
//        mqttConnectOptions.setUserName(username);
//        mqttConnectOptions.setPassword(password.toCharArray());
//        return mqttConnectOptions;
//    }
//
//    private MqttAndroidClient createMqttAndroidClient() {
//        //create and return client
//        return new MqttAndroidClient(getApplicationContext(),serverUri,clientId);
//    }
//
//    public void connect(final MqttAndroidClient client, MqttConnectOptions options) {
//
//        try {
//            if (!client.isConnected()) {
//                IMqttToken token = client.connect(options);
//                //on successful connection, publish or subscribe as usual
//                token.setActionCallback(new IMqttActionListener() {
//
//                    @Override
//                    public void onSuccess(IMqttToken asyncActionToken) {
//                        Log.d("MQTTmanager","Connected Successfully by Thinh");
//                    }
//
//                    @Override
//                    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
//                        Log.d("MQTTmanager","FAILED TO CONNECT: NGU");
//                    }
//                });
//                client.setCallback(new MqttCallback() {
//                    @Override
//                    public void connectionLost(Throwable cause) {
//                        Log.d("MQTTClient:","Lost connection with cause: " + cause.getMessage() + " - Thinh");
//                    }
//
//                    @Override
//                    public void messageArrived(String topic, MqttMessage message) throws Exception {
//                        Log.d("MQTT Client:","This is basic primal callback handler-Thinh");
//                    }
//
//                    @Override
//                    public void deliveryComplete(IMqttDeliveryToken token) {
//                        Log.d("MQTT Client:","Done delivery=Thinh :V");
//                    }
//                });
//            }
//        } catch (MqttException e) {
//            //handle e
//            e.printStackTrace();
//        }
//    }
//    public void setNewClientCallBack(MqttCallback callback){
//        client.setCallback(callback);
//    }
//}