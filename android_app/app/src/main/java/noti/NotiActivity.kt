    package noti

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.Image
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager

import com.example.myapplication.R
import com.example.myapplication.databinding.ActivityIntrusionHistoryBinding
import com.example.myapplication.databinding.ActivityNotiBinding
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dataclasses.DoorDevice
import door.DoorViewModel
import intrusionhistory.IntrusionHistoryRecyclerAdapter
import intrusionhistory.IntrusionViewModel
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended
import org.eclipse.paho.client.mqttv3.MqttMessage
import repository.Repository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.time.LocalDateTime
import java.util.*
import kotlin.collections.ArrayList
import kotlin.random.Random.Default.nextInt

    const val SECU_LEVEL_CHANGE_NOTI: String = "Security level Change"
const val INSTRUSION_NOTI: String = "Intrusion detected"
const val SPEAKER_HIGH_VALUE = "1023"

class NotiActivity : AppCompatActivity() {
    private lateinit var toolbar: Toolbar
    private lateinit var binding: ActivityNotiBinding
    private lateinit var viewModel: NotiViewModel
    private val gson = Gson();
    private lateinit var notiHandler: MQTTNotiHandler
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_noti)
        setUpToolBar()

        binding = DataBindingUtil.setContentView(this, R.layout.activity_noti)
        viewModel = ViewModelProvider(this).get(NotiViewModel::class.java)
        val layoutManager = LinearLayoutManager(this)
        binding.notificationRecylerView.layoutManager = layoutManager
        viewModel.getNoti().observe(this, Observer { notiList ->
            if (notiList == null) {
                binding.notificationRecylerView.visibility = View.GONE
            } else binding.notificationRecylerView.adapter = NotificationRecyclerAdapter(notiList)
        })

        val notiViewModel = ViewModelProvider(this).get(NotiViewModel::class.java)

        notiViewModel.getNoti().observe(this, Observer { notiList ->
            var result: String = ""
            for (notiItem in notiList) {
                result += "notiID: ${notiItem.id} \n notiTime: ${notiItem.time} \n" +
                        " notiDoorId: ${notiItem.doorId}\n\n"

            }
            Log.d("STAT_RESULT:", "$result")
            Log.d("NOTI_LIST: ", "$notiList")
            //textView.text = result
        })

        subscribeToTopics(Repository.topicList)
        // TODO: REMOVE THE TWO BUTTON AFTER DONE TESTING (in xml as well)
        // Turn speaker on
//        findViewById<Button>(R.id.test_pub_speaker_button).setOnClickListener{
//            val message ="{\"id\":\"2\",\"name\":\"SPEAKER\",\"data\":\"1023\",\"unit\":\"\"}"
//            publishToAda("thanhngodinh/feeds/speaker",message)
//            Log.d("MESS_PUBLISHED:",message)
//        }
        // Turn speaker off
//        findViewById<Button>(R.id.test_pub_speaker_button_2).setOnClickListener{
//            val message ="{\"id\":\"2\",\"name\":\"SPEAKER\",\"data\":\"0\",\"unit\":\"\"}"
//            publishToAda("thanhngodinh/feeds/speaker",message)
//            Log.d("MESS_PUBLISHED:",message)
//        }

        notiHandler = MQTTNotiHandler(this)
    }
    //TODO: REMOVE THE BELOW FUNCTION AFTER TESTING
//    private fun publishToAda(topic:String, message: String){
//        Repository.mqttService.mqttAndroidClient.publish(topic, MqttMessage(message.toByteArray()))
//    }


    private fun subscribeToTopics(topicList: ArrayList<String>) {
        Repository.mqttService.setCallBack(object : MqttCallbackExtended {
            override fun connectComplete(reconnect: Boolean, serverURI: String) {}
            override fun connectionLost(cause: Throwable) {}

            @RequiresApi(Build.VERSION_CODES.O)
            @Throws(Exception::class)
            override fun messageArrived(topic: String, message: MqttMessage) {
                Log.d("MQTT_MESS:",message.toString())
                // ample: {"id":"2","name":"SPEAKER","data":"1023","unit":""}
                notiHandler.handleMessage(message)

                val device: DoorDevice = gson.fromJson(message.toString(),DoorDevice::class.java)

                if(device.name == "SPEAKER"){
                    // Khuc nay co the sai (Ve mat du lieu thoi) Do thong tin trong MQTT khong co DOOR ID. T lam mau thoi m thao luan voi tho lam laij cho dung
                    val rnds = (0..1000000).random()
                    viewModel.addNewNoti(rnds.toString(), "1", LocalDateTime.now().toString())
                }
                // TODO: Update the List Items


            }

            override fun deliveryComplete(token: IMqttDeliveryToken) {}
        })
    }


    // Set up the behavior when the toolbar back arrow is pressed
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }


    private fun setUpToolBar() {
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val actionbar: ActionBar? = supportActionBar
        actionbar?.setDisplayHomeAsUpEnabled(true)
        actionbar?.setDisplayShowHomeEnabled(true)

        toolbar.setBackgroundColor(getColor(R.color.notification_theme))
    }

}