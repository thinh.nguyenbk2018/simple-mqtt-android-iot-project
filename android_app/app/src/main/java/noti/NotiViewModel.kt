package noti

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dataclasses.Notification
import repository.Repository

class NotiViewModel() : ViewModel() {
    private lateinit var notiLiveData : MutableLiveData<ArrayList<Notification>>
    fun getNoti() : LiveData<ArrayList<Notification>>{
        notiLiveData = Repository.getNotiLiveData()
        return notiLiveData
    }
    fun addNewNoti(id: String, doorID: String, timee : String){
        var notiList = notiLiveData.value
        if (notiList != null) {
            notiList.add(0, Notification(iid = id,idoorID = doorID,itime = timee)) //add vao dau duoc a.
        }
        notiLiveData.postValue(notiList)
//        return notiLiveData
    }
} //chay duoc i. nhg mà đúng ý không ?
//no nhay dung y. Nhung ma sau do no tro ve lai nhu cu.
//No reload them cai doorID = 2 vao tren cung, duoc khoang 1/10s, no tro ve lai nhu cu.
//T nghi la do viewModel no load lai.
//ViewModel o tren load lai. ừ. để t 10p
// Cai noti cua m can gi the: Ý là mỗi noti cần cái gì á
//Thin: Chi can cai doorID thoi/ thế time khoogn lấy à
//Co lay het. Nhung t chi de doorID vao xml thoi. ok
// M co bat tieng viet khoong co thi tat di cai
// Nhu nao ?
//Ngon roi, gio reverse cai list lai nua la ngon luon. Reverse ton tien lam
//oke, vay de nhu vay luon. Khong add vao dau duoc maf
//t nhan 1 cai, m nhan 1 cai => no ra 7.
//Hien thi len dau list luon roi
//Ngon roi . u de t 5p