package noti

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.databinding.NotifyItemBinding
import dataclasses.Notification



class NotificationRecyclerAdapter(private val notiList : ArrayList<Notification>) : RecyclerView.Adapter<NotificationRecyclerAdapter.NotificationItemViewHolder>(){
    class NotificationItemViewHolder private constructor(private val binding: NotifyItemBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(item : Notification){
            binding.noti = item
        }
        companion object {
            fun from(parent: ViewGroup) : NotificationItemViewHolder{
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = NotifyItemBinding.inflate(layoutInflater,parent,false)
                return NotificationItemViewHolder(binding)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationItemViewHolder {
        //TODO("Not yet implemented")
        return NotificationItemViewHolder.from(parent)

    }

    override fun onBindViewHolder(holder: NotificationItemViewHolder, position: Int) {
        //TODO("Not yet implemented")
        holder.bind(notiList[position])

    } // Toi cho co cai list noti do. List cua cai adapter nay tren app ay. Roi thi nhan phim  sao roi ?
    //Nhan roi, no tu 5 len 6 roi len 7. U .ben  app t t pub len day
    //t cung gui 1 cai. Roi no hoat dong dung chua  ?. la tren kia len item khong

    //co len. hoatj dong dung y. Nhung phai reverse lai. Vi no them vao cuoi list. co ra door id khong ?
    //Ra null door, vi doorID dang la null. (voi cai moi them vao)
    override fun getItemCount(): Int {
        //TODO("Not yet implemented")
        Log.d("THINH_DEBUG:","NOTI LIST COUNT: ${notiList.size}")
        return notiList.size
    }
}

