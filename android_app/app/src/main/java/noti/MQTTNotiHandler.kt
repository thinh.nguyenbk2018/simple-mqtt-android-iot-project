package noti

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat.getSystemService
import com.example.myapplication.R
import com.google.gson.Gson
import dataclasses.DoorDevice
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.MqttCallback
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended
import org.eclipse.paho.client.mqttv3.MqttMessage
import repository.Repository

private const val CHANEL_ID = "CHANNEL_1"
private const val notiID = 101
private val gson = Gson()

class MQTTNotiHandler(private val context: Context): ContextWrapper(context) {
    init{
        Log.d("NOTI_BOY:","My cape is obtained. Now I can fly")
        createNotificationChannel()
    }
    fun setCallbackAndHandle(){
        Repository.mqttService.setCallBack(object:MqttCallbackExtended{
            override fun connectionLost(cause: Throwable?) {
            }

            override fun messageArrived(topic: String?, message: MqttMessage?) {
                Log.d("NOTI_BOY_BACK:","Handling MQTT with callback set")
                if (message != null) {
                    handleMessage(message)
                }
            }

            override fun deliveryComplete(token: IMqttDeliveryToken?) {
            }

            override fun connectComplete(reconnect: Boolean, serverURI: String?) {
            }

        })
    }
    fun handleMessage(message: MqttMessage){
        val device: DoorDevice = gson.fromJson(message.toString(), DoorDevice::class.java)
        if (device.name == "SPEAKER") {
            if (device.data == SPEAKER_HIGH_VALUE) {
                Log.d("NOTI_BOY","Sending noti speaker")
                sendNotification(type = INSTRUSION_NOTI, value = device.data)
            }
        } else if (device.name == "LED") {
            // Handle Security level change
            sendNotification(SECU_LEVEL_CHANGE_NOTI, device.data)
        }
    }
    private fun createNotificationChannel(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val name = "Change security level"
            val descriptionText  = "Security level was changed"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANEL_ID, name, importance).apply {
                description = descriptionText
            }
            val notificationManager : NotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
    private fun sendNotification(type: String, value: String) {
        Log.d("SENDING NOTI:", "type: $type, value: $value")
        val intent = Intent(context, NotiActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntent: PendingIntent = PendingIntent.getActivity(context, 0, intent, 0)
        val bitmap: Bitmap =
            BitmapFactory.decodeResource(context.resources, R.drawable.cat_boy)
        val bitmapLargeIcon: Bitmap =
            BitmapFactory.decodeResource(context.resources, R.drawable.cat_boy)
        if (type == SECU_LEVEL_CHANGE_NOTI) {
            val level = if (value == "2") "ON" else "OFF"
            val builder = NotificationCompat.Builder(context, CHANEL_ID)
                .setSmallIcon(R.drawable.favicon)
                .setLargeIcon(bitmapLargeIcon)
                .setStyle(NotificationCompat.BigPictureStyle().bigPicture(bitmap))
                .setContentIntent(pendingIntent)
                .setContentTitle("Security Level Changed")
                .setContentText("Security level was Turned $level")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            with(NotificationManagerCompat.from(context)) {
                notify(notiID, builder.build())
            }
        } else {
            val builder = NotificationCompat.Builder(context, CHANEL_ID)
                .setSmallIcon(R.drawable.favicon)
                .setLargeIcon(bitmapLargeIcon)
                .setStyle(NotificationCompat.BigPictureStyle().bigPicture(bitmap))
                .setContentIntent(pendingIntent)
                .setContentTitle("Intrusion detected")
                .setContentText("Bớ người ta. Có thằng ăn trộm.")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            with(NotificationManagerCompat.from(context)) {
                notify(notiID, builder.build())
            }
        }

    }
}