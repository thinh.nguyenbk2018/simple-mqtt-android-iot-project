package stat 

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dataclasses.Stat
import repository.Repository


class StatViewModel() : ViewModel(){
    private lateinit var statLiveData : MutableLiveData<ArrayList<Stat>>
     
    fun getStat() : LiveData<ArrayList<Stat>> {
        statLiveData = Repository.getStatLiveData()
        return statLiveData
    }
     
}