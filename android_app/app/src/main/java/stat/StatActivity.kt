package stat

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.graphics.Color
import android.media.Image
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.*
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import apitest.TestStringService
import apitest.TestWeatherAPIActivity
import apitest.TestWeatherAPIActivity.Companion.BaseUrl

import com.example.myapplication.R
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.utils.ColorTemplate
import com.google.gson.GsonBuilder
import door.DoorViewModel
import noti.MQTTNotiHandler
import org.w3c.dom.DOMErrorHandler
import org.w3c.dom.Text
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.NumberFormatException
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList

class StatActivity() : AppCompatActivity(), DatePickerDialog.OnDateSetListener {

    //private lateinit var fetchButton: Button
    private lateinit var textView: TextView
    lateinit var barList: ArrayList<BarEntry>
    lateinit var barDataSet: BarDataSet
    lateinit var barData: BarData
    private lateinit var toolbar: androidx.appcompat.widget.Toolbar

    //    lateinit var xvalues : ArrayList<String>
    val result = mutableListOf("")
    private lateinit var notiHandler: MQTTNotiHandler
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.stat)
        setUpToolBar()
//        val doorViewModel = ViewModelProvider(this).get(DoorViewModel::class.java)

        val statViewModel = ViewModelProvider(this).get(StatViewModel::class.java)
        //fetchButton = findViewById(R.id.fetchButton)
        textView = findViewById(R.id.textViewStat)
        statViewModel.getStat().observe(this, Observer { statList ->

        if(result.size == 1) {
            for (statItem in statList) {
                //result += "statID: ${statItem.id.toString()} \nstatTime: ${(statItem.time).toString()} \n\n"
                //result += statItem.time.toInt()
                result.add(statItem.time.toString())
            }
            result.removeAt(0)
        }



            Log.d("STAT_RESULT:", "$result")


        })
        pickDate()
        // Register noti
        notiHandler = MQTTNotiHandler(this)
        notiHandler.setCallbackAndHandle()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun setData() {

//
//        val firstApiFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
//        val date = LocalDate.parse("2019-08-07 09:00:00" , firstApiFormat)

        //Convert from epoch time fo YYYY-MM-DD
        val timestamp = (result[3].toLong() / 1000).toLong() // timestamp in Long
        val timestampAsDateString = java.time.format.DateTimeFormatter.ISO_INSTANT
            .format(java.time.Instant.ofEpochSecond(timestamp))

        //Convert YYYY-MM-DD to epoch time
        var dateFromString = ""
        if (monthfrom < 10 && dayfrom >= 10) {
            dateFromString = "$yearfrom-0$monthfrom-$dayfrom 00:00:00"
        }
        if (monthfrom < 10 && dayfrom < 10) {
            dateFromString = "$yearfrom-0$monthfrom-0$dayfrom 00:00:00"
        }
        if (monthfrom >= 10 && dayfrom >= 10) {
            dateFromString = "$yearfrom-$monthfrom-$dayfrom 00:00:00"
        }
        if (monthfrom >= 10 && dayfrom < 10) {
            dateFromString = "$yearfrom-$monthfrom-0$dayfrom 00:00:00"
        }

        //val now = LocalDateTime.now(ZoneOffset.UTC)
        val dateFromStringFormatted =
            LocalDateTime.parse(dateFromString, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
        // LocalDateTime to epoch seconds
        // val seconds = now.atZone(ZoneOffset.UTC).toEpochSecond()
        val dateFromStringEpoch = dateFromStringFormatted.atZone(ZoneOffset.UTC).toEpochSecond()
        // Epoch seconds to LocalDateTime
        //val newNow = LocalDateTime.ofInstant(Instant.ofEpochSecond(seconds), ZoneOffset.UTC)


        Log.d("parseTestingDate1: ", "$dateFromStringFormatted")
        Log.d("parseTestingEpoch1: ", "$dateFromStringEpoch")

        //Convert YYYY-MM-DD to epoch time
        var dateToString = ""
        if (tomonth < 10 && today >= 10) {
            dateToString = "$toyear-0$tomonth-$today 00:00:00"
        }
        if (tomonth < 10 && today < 10) {
            dateToString = "$toyear-0$tomonth-0$today 00:00:00"
        }
        if (tomonth >= 10 && today >= 10) {
            dateToString = "$toyear-$tomonth-$today 00:00:00"
        }
        if (tomonth >= 10 && today < 10) {
            dateToString = "$toyear-$tomonth-0$today 00:00:00"
        }

        //val now = LocalDateTime.now(ZoneOffset.UTC)
        val dateToStringFormatted =
            LocalDateTime.parse(dateToString, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
        // LocalDateTime to epoch seconds
        // val seconds = now.atZone(ZoneOffset.UTC).toEpochSecond()
        val dateToStringEpoch = dateToStringFormatted.atZone(ZoneOffset.UTC).toEpochSecond()
        // Epoch seconds to LocalDateTime
        //val newNow = LocalDateTime.ofInstant(Instant.ofEpochSecond(seconds), ZoneOffset.UTC)


        Log.d("parseTestingDate1: ", "$dateToStringFormatted")
        Log.d("parseTestingEpoch1: ", "$dateToStringEpoch")
        var listChosenDate = mutableListOf<Long>()
        for (i in listOf(1, 2, 3, 4, 5, 6, 7)) {
            if (dateFromStringEpoch + 86400 * i <= dateToStringEpoch + 86400) {
                listChosenDate.add(dateFromStringEpoch + 86400 * i - 86400)
                val newNow = LocalDateTime.ofInstant(
                    Instant.ofEpochSecond(dateFromStringEpoch + 86400 * i - 86400),
                    ZoneOffset.UTC
                )
                Log.d("Test yyyddmm: ", "$newNow")
            } else {
                break
            }
        }
        Log.d("Range_Chosen_Date: ", "$listChosenDate")

        var listEntrancePerDay = mutableListOf<Int>()

        for (i in listChosenDate) {
            var count = 0
            for (item in result) {
                try {
                    if (item.toLong() / 1000 > i && item.toLong() / 1000 <= i + 86400) {
                        count += 1
                    }
                } catch (e: NumberFormatException) {
                    Log.d("CONVERT_EXCEPTION", "Crash ho bo m cai")
                }
            }
            listEntrancePerDay.add(count)

        }


        Log.d("ListEntrancePerDay: ", "$listEntrancePerDay")

        barList = ArrayList()
        var xis = 0f
        for (x in listEntrancePerDay) {
            xis += 10f
            barList.add(BarEntry(xis, x.toFloat()))
        }
//        barList.add(BarEntry(10f, 5f))
//        barList.add(BarEntry(20f, 30f))
//        barList.add(BarEntry(30f, 10f))
//        barList.add(BarEntry(40f, 70f))
//        barList.add(BarEntry(50f, 50f))
//        barList.add(BarEntry(60f, 20f))
//        barList.add(BarEntry(70f, 40f))

        barDataSet = BarDataSet(barList, "Entrances")

        barData = BarData(barDataSet)
        barData.barWidth = 5f

        val barChart = findViewById<BarChart>(R.id.barChartStat)

        barChart.data = barData
//        val x = listOf("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
//        barChart.xAxis.valueFormatter  =  IndexAxisValueFormatter(x)
//        barChart.xAxis.textColor = Color.BLACK
        barDataSet.setColor(Color.argb(250,155,221,255))

        barDataSet.valueTextColor = Color.BLACK
        barDataSet.valueTextSize = 15f

    }

    fun back() {

    }

    var dayfrom = 0
    var monthfrom = 0
    var yearfrom = 0

    var savedDay = 0
    var savedMonth = 0
    var savedYear = 0

    var today = 0
    var tomonth = 0
    var toyear = 0

    private fun getDateCalendar() {
        val cal = Calendar.getInstance()
        dayfrom = cal.get(Calendar.DAY_OF_MONTH)
        monthfrom = cal.get(Calendar.MONTH)
        yearfrom = cal.get(Calendar.YEAR)

        today = cal.get(Calendar.DAY_OF_MONTH)
        tomonth = cal.get(Calendar.MONTH)
        toyear = cal.get(Calendar.YEAR)
        if ((today - dayfrom > 7) or (tomonth != monthfrom)) {
            Log.d("Range: ", (today - dayfrom).toString())
            Toast.makeText(this, "It's just around 7 days", Toast.LENGTH_SHORT).show()
        }
    }

    var fromBool = false
    var fromTo = false
    private fun pickDate() {
        val btn_pick_from_date: Button = findViewById<Button>(R.id.btn_pick_from_date)
        btn_pick_from_date.setOnClickListener {
            fromBool = true
            val cal = Calendar.getInstance()
            dayfrom = cal.get(Calendar.DAY_OF_MONTH)
            monthfrom = cal.get(Calendar.MONTH)
            yearfrom = cal.get(Calendar.YEAR)
            DatePickerDialog(this, this, yearfrom, monthfrom, dayfrom).show()
        }
        val btn_pick_to_date = findViewById<Button>(R.id.btn_pick_to_date)
        btn_pick_to_date.setOnClickListener {
            fromTo = true
            val cal = Calendar.getInstance()
            today = cal.get(Calendar.DAY_OF_MONTH)
            tomonth = cal.get(Calendar.MONTH)
            toyear = cal.get(Calendar.YEAR)
            DatePickerDialog(this, this, toyear, tomonth, today).show()
        }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        //TODO("Not yet implemented")
        savedDay = dayOfMonth
        savedMonth = month + 1
        savedYear = year

        if (fromBool) {
            var txtfromday: TextView = findViewById<TextView>(R.id.txt_fromday)
            txtfromday.text = "$savedDay - $savedMonth - $savedYear"
            dayfrom = savedDay
            monthfrom = savedMonth
            yearfrom = savedYear
            fromBool = false
            //Toast.makeText(this, "It's just around 7 days", Toast.LENGTH_SHORT).show()


        }
        if (fromTo) {
            var txt_today: TextView = findViewById<TextView>(R.id.txt_today)
            txt_today.text = "$savedDay - $savedMonth - $savedYear"
            today = savedDay
            tomonth = savedMonth
            toyear = savedYear
            fromTo = false
            var dateFromString = ""
            if (monthfrom < 10 && dayfrom >= 10) {
                dateFromString = "$yearfrom-0$monthfrom-$dayfrom 00:00:00"
            }
            if (monthfrom < 10 && dayfrom < 10) {
                dateFromString = "$yearfrom-0$monthfrom-0$dayfrom 00:00:00"
            }
            if (monthfrom >= 10 && dayfrom >= 10) {
                dateFromString = "$yearfrom-$monthfrom-$dayfrom 00:00:00"
            }
            if (monthfrom >= 10 && dayfrom < 10) {
                dateFromString = "$yearfrom-$monthfrom-0$dayfrom 00:00:00"
            }

            //val now = LocalDateTime.now(ZoneOffset.UTC)
            val dateFromStringFormatted = LocalDateTime.parse(
                dateFromString,
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
            )
            // LocalDateTime to epoch seconds
            // val seconds = now.atZone(ZoneOffset.UTC).toEpochSecond()
            val dateFromStringEpoch = dateFromStringFormatted.atZone(ZoneOffset.UTC).toEpochSecond()
            // Epoch seconds to LocalDateTime
            //val newNow = LocalDateTime.ofInstant(Instant.ofEpochSecond(seconds), ZoneOffset.UTC)


            Log.d("parseTestingDate1: ", "$dateFromStringFormatted")
            Log.d("parseTestingEpoch1: ", "$dateFromStringEpoch")

            //Convert YYYY-MM-DD to epoch time
            var dateToString = ""
            if (tomonth < 10 && today >= 10) {
                dateToString = "$toyear-0$tomonth-$today 00:00:00"
            }
            if (tomonth < 10 && today < 10) {
                dateToString = "$toyear-0$tomonth-0$today 00:00:00"
            }
            if (tomonth >= 10 && today >= 10) {
                dateToString = "$toyear-$tomonth-$today 00:00:00"
            }
            if (tomonth >= 10 && today < 10) {
                dateToString = "$toyear-$tomonth-0$today 00:00:00"
            }
            // M vo cai stat test lai xem con crash khong
            //T test may lan roi, k bi nua. Bua bi la do format sai.
            //h ok roi. k crash nua. U nhung gio khong chac dau. M dang khong crash cho server phien ban cu (Luc demo sau giua ky day).
            // Comment cho t cho m fix cho no hien tai khong crash di
            //val now = LocalDateTime.now(ZoneOffset.UTC)
            val dateToStringFormatted = LocalDateTime.parse(
                dateToString,
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
            )
            // LocalDateTime to epoch seconds
            // val seconds = now.atZone(ZoneOffset.UTC).toEpochSecond()
            val dateToStringEpoch = dateToStringFormatted.atZone(ZoneOffset.UTC).toEpochSecond()
            // Epoch seconds to LocalDateTime
            //val newNow = LocalDateTime.ofInstant(Instant.ofEpochSecond(seconds), ZoneOffset.UTC)
            if (dateToStringEpoch - dateFromStringEpoch > 7 * 86400) {
                //Toast.makeText(this, "It's just around 7 days", Toast.LENGTH_SHORT).show()
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Out of the range of time")
                val dateString = "$yearfrom-$monthfrom-$dayfrom 00:00:00"
                builder.setMessage("Just around in 7 days: You chose ${(dateToStringEpoch - dateFromStringEpoch) / 86400} days. Please choose again.")

                builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                    Toast.makeText(
                        applicationContext,
                        android.R.string.yes, Toast.LENGTH_SHORT
                    ).show()
                }

                builder.setNegativeButton(android.R.string.no) { dialog, which ->
                    Toast.makeText(
                        applicationContext,
                        android.R.string.no, Toast.LENGTH_SHORT
                    ).show()
                }

                builder.setNeutralButton("Maybe") { dialog, which ->
                    Toast.makeText(
                        applicationContext,
                        "Maybe", Toast.LENGTH_SHORT
                    ).show()
                }
                builder.show()
            }
            setData()

        }

    }

    // Set up the behavior when the toolbar back arrow is pressed
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }


    private fun setUpToolBar() {
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val actionbar: ActionBar? = supportActionBar
        actionbar?.setDisplayHomeAsUpEnabled(true)
        actionbar?.setDisplayShowHomeEnabled(true)

        toolbar.setBackgroundColor(getColor(R.color.stat_theme))
    }
}