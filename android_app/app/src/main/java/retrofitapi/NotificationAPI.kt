package retrofitapi

import dataclasses.Notification
import retrofit2.Call
import retrofit2.http.GET

interface NotificationAPI {

    @GET("notification")
    fun getNotification() : Call<ArrayList<Notification>>

}