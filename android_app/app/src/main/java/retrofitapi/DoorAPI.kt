package retrofitapi

import dataclasses.Door
import dataclasses.DoorBatchCommand
import retrofit2.Call
import retrofit2.http.*

interface DoorAPI {
    // Return status of all doors
    @GET("door-status")
    fun getAllDoorState() : Call<ArrayList<Door>>

    // Get status of specific door: door_ID
    @GET("door-status/{id}")
    fun getDoorStateWithID(@Path("id") id: String) : Call<Door>

    // Close Specific Door:  0 --> Close the Door  1: Open the Door
    @PATCH("door-status/{id}")
    fun changeDoorStateWithID(@Path("id") id: String, @Body door: Door) : Call<Door>

    // Close/Open All Door:
    @PATCH("door-status/all")
    fun changeAllDoorState(@Body command: DoorBatchCommand) : Call<ArrayList<Door>>
}

