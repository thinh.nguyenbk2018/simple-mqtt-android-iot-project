package retrofitapi

import apitest.TestWeatherAPIActivity
import com.google.gson.GsonBuilder
import okhttp3.OkHttp
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

// SINGLE TON RETROFIT OBJECT FOR CREATING THE APIs
object RetrofitSingleton {
    // Base Url with IP address and port
    private const val BASE_URL = "http://52.163.201.3:3000"
    private val gson = GsonBuilder()
        .setLenient()
        .create()

    private val loggingInterceptor = HttpLoggingInterceptor()
        .setLevel(HttpLoggingInterceptor.Level.BODY)

    private val okHttpClient = OkHttpClient.Builder().addInterceptor(loggingInterceptor).build()

    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(okHttpClient)
        .build()

    val retrofitDoorAPI : DoorAPI by lazy{
        retrofit.create(DoorAPI::class.java)
    }

    val retrofitIntrusionAPI : IntrusionAPI by lazy{
        retrofit.create(IntrusionAPI:: class.java)
    }

    val retrofitUserAPI : UserAPI by lazy{
        retrofit.create(UserAPI::class.java)
    }

    val retrofitStatAPI : StatAPI by lazy{
        retrofit.create(StatAPI::class.java)
    }

    val retrofitNotificationAPI : NotificationAPI by lazy {
        retrofit.create(NotificationAPI::class.java)
    }
    // change security level
    val retrofitSecurityAPI: SecurityAPI by lazy{
        retrofit.create(SecurityAPI::class.java)
    }

}