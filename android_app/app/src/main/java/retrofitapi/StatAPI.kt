package retrofitapi

import dataclasses.Stat
import retrofit2.Call
import retrofit2.http.*

interface StatAPI {
    // Return status of all doors
    @GET("entrance-stat")
    fun getAllEntranceStat() : Call<ArrayList<Stat>>

    // Get status of specific door: door_ID
    @GET("door-status/{id}")
    fun getStatWithID() : Call<Stat>

}