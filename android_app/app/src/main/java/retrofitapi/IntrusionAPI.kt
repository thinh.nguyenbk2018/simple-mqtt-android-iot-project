package retrofitapi

import dataclasses.Door
import dataclasses.Intrusion
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface IntrusionAPI {
    // Get all intrusion logged
//    @Header("Token:")
    @GET("intrusion-history")
    fun getAllDoorIntrusion(@Header("access-token") token :String) : Call<ArrayList<Intrusion>>

    // Get status of specific door: door_ID
    @GET("intrusion-history/{id}")
    fun getDoorIntrusionWithID(@Header("access-token") token :String,@Path("id") id: String) : Call<ArrayList<Intrusion>>

    // Get status of specific door: door_ID with time in Query
    @GET("intrusion-history/{id}")
    fun getDoorIntrusionWithIDinInterval(@Header("access-token") token :String,@Path("id") id: String, @Query("from") from: String, @Query("to") to: String) : Call<ArrayList<Intrusion>>

}