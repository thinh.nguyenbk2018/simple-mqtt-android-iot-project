package retrofitapi

import android.os.Message
import dataclasses.MessageWithToken
import dataclasses.MessageWithTokenAndTopics
import dataclasses.UserRelatedResponse
import dataclasses.User
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface UserAPI {
    // This method is for registering:  Creating a new user
    @POST("users")
    fun createUser(@Body user: User) : Call<UserRelatedResponse>
    // This method is for login: getting back a Access Token
    @POST("users/auth")
    fun authenticateUser(@Body user:User): Call<MessageWithTokenAndTopics>


    //TODO: Test method with access-token : Expect responsee with message user
//    @GET("user")
//    Call<User> getUser(@Header("Authorization") String authorization)
    @GET("users")
    fun testGetWithToken(@Header("access-token") token :String) : Call<String>

}