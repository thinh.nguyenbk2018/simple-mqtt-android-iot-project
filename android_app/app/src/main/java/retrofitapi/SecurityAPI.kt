package retrofitapi

import dataclasses.Security
import dataclasses.SecurityCommand
import retrofit2.Call
import retrofit2.http.*

interface SecurityAPI {
    @GET("security")
    fun getSecurityLevel(): Call<String>

    @PUT("security")
    fun setSecurityLevel(@Header("access-token") token: String,@Body command: SecurityCommand): Call<String>
}