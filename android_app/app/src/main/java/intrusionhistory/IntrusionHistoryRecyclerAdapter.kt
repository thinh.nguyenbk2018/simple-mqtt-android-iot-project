package intrusionhistory

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.databinding.IntrusionItemBinding
import dataclasses.Intrusion
import door.doorintrusion.DoorIntrusionDetailRecyclerAdapter
import java.util.*
import kotlin.collections.ArrayList

class IntrusionHistoryRecyclerAdapter(private val intrusionList : ArrayList<Intrusion>
)
    : RecyclerView.Adapter<IntrusionHistoryRecyclerAdapter.IntrusionItemViewHolder>(){
    class IntrusionItemViewHolder private constructor(private val binding : IntrusionItemBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(item: Intrusion){
            item.time = item.time?.let { getDateTimeFromEpocLongOfSeconds(it) }
            binding.intrusionItem = item
        }
        companion object {
            fun from(parent: ViewGroup) : IntrusionItemViewHolder{
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = IntrusionItemBinding.inflate(layoutInflater,parent, false)
                return IntrusionItemViewHolder(binding)
            }
        }
        private fun getDateTimeFromEpocLongOfSeconds(epoc: String): String? {
            return try {
                val netDate = Date(epoc.toLong())
                netDate.toString()
            } catch (e: Exception) {
                e.toString()
            }
        }
    }
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): IntrusionHistoryRecyclerAdapter.IntrusionItemViewHolder {
        return IntrusionItemViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: IntrusionHistoryRecyclerAdapter.IntrusionItemViewHolder, position: Int) {
        holder.bind(intrusionList[position])
    }

    override fun getItemCount(): Int {
        return intrusionList.size
    }

}
