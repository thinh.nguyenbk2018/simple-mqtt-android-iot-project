package intrusionhistory

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import android.widget.Toolbar
import androidx.appcompat.app.ActionBar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.R
import com.example.myapplication.databinding.ActivityIntrusionHistoryBinding
import door.doorintrusion.DoorIntrusionDetailRecyclerAdapter
import noti.MQTTNotiHandler

class IntrusionHistoryActivity : AppCompatActivity() {
    private lateinit var binding : ActivityIntrusionHistoryBinding
    private lateinit var toolbar: androidx.appcompat.widget.Toolbar
    private lateinit var viewModel: IntrusionViewModel
    private lateinit var notiHandler: MQTTNotiHandler
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_intrusion_history)
        setupToolBar()
        viewModel = ViewModelProvider(this).get(IntrusionViewModel::class.java)

        val layoutManager = LinearLayoutManager(this)
        binding.intrusionHistoryRecyclerView.layoutManager = layoutManager
        viewModel.getAllIntrusion().observe(this, Observer {intrusionList->
            if(intrusionList == null){
                Toast.makeText(this,"Bảo vệ quèn không có quyền xem log của chúng tôi. \ud83d\udeab ", Toast.LENGTH_SHORT).show()
                binding.intrusionHistoryRecyclerView.visibility = View.GONE
            }
            else binding.intrusionHistoryRecyclerView.adapter = IntrusionHistoryRecyclerAdapter(intrusionList)
        })
        // Register noti
        notiHandler = MQTTNotiHandler(this)
        notiHandler.setCallbackAndHandle()
    }

    private fun setupToolBar(){
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val actionbar: ActionBar? = supportActionBar
        actionbar?.setDisplayHomeAsUpEnabled(true)
        actionbar?.setDisplayShowHomeEnabled(true)

        // Change the toolbar color
        binding.toolbar.setBackgroundColor(getColor(R.color.intrusion_history_color_theme))
    }

    // Set up the behavior when the toolbar back arrow is pressed
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }


}