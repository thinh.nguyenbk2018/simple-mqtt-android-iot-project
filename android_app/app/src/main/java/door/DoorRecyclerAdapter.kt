package door

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.databinding.DoorStateItemBinding
import dataclasses.Door


const val DOOR_CLOSE_STRING = "1"
const val BUTTON_CLOSE_STRING = "CLOSE"
const val BUTTON_OPEN_STRING = "OPEN"

class DoorClickListener(val clickListener: (door : Door) -> Unit){
    fun onClick(door: Door) = clickListener(door)
}
class ButtonClickListener(val clickListener: (door : Door) -> Unit){
    fun onClick(door: Door) = clickListener(door)
}
class DoorRecyclerAdapter(private val doorList : ArrayList<Door>, private val clickListener: DoorClickListener,private val buttonClickListener: ButtonClickListener) : RecyclerView.Adapter<DoorRecyclerAdapter.DoorItemViewHolder>(){
    class DoorItemViewHolder private constructor(private val binding : DoorStateItemBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(item: Door, clickListener: DoorClickListener, buttonListener: ButtonClickListener){
            binding.doorItem = item
            binding.doorClickListener = clickListener
            binding.buttonClickListener = buttonListener


            binding.doorImage.setImageResource(getDoorImageFromState(item.state))

            if(item.state == DOOR_CLOSE_STRING){
                binding.buttonChangeDoorState.text = BUTTON_CLOSE_STRING
                binding.buttonChangeDoorState.setBackgroundResource(R.drawable.reddish_button_background)
            }
            else{
                binding.buttonChangeDoorState.text = BUTTON_OPEN_STRING
                binding.buttonChangeDoorState.setBackgroundResource(R.drawable.thinhbutton)
            }
//            binding.buttonChangeDoorState.text = if(item.state == DOOR_CLOSE_STRING) "CLOSE" else "OPEN"
//            val color = if(item.state == DOOR_CLOSE_STRING) Color.RED else Color.GREEN
//            Log.d("DOOR_ADAPTER_COLOR","$color")
//            binding.buttonChangeDoorState.setBackgroundColor(Color.RED)

//            var buttonDrawable = binding.buttonChangeDoorState.background
//            buttonDrawable = DrawableCompat.wrap(buttonDrawable)
//            DrawableCompat.setTint(buttonDrawable,color)
//            binding.buttonChangeDoorState.background = buttonDrawable

        }
        private fun getDoorImageFromState(state : String?) :Int{
            return when (state) {
                null -> R.drawable.door_open_drawable
                DOOR_CLOSE_STRING -> {
                    R.drawable.door_close_drawable_new
                }
                else -> R.drawable.door_open_drawable
            }
        }
        companion object {
            fun from(parent: ViewGroup) : DoorItemViewHolder{
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = DoorStateItemBinding.inflate(layoutInflater,parent, false)
                return DoorItemViewHolder(binding)
            }
        }
    }
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DoorRecyclerAdapter.DoorItemViewHolder {
        return DoorItemViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: DoorRecyclerAdapter.DoorItemViewHolder, position: Int) {
        holder.bind(doorList[position],clickListener,buttonClickListener)
    }

    override fun getItemCount(): Int {
        Log.d("DOOR_ADAPTER:","DOORLIST: ${doorList.toString()}")
        return doorList.size
    }

}