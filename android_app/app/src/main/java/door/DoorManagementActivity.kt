package door

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.R
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.example.myapplication.databinding.ActivityDoorManagementBinding
import com.google.gson.Gson
import dataclasses.CLOSE_ALL_DOOR
import dataclasses.Door
import dataclasses.DoorDevice
import dataclasses.OPEN_ALL_DOOR
import door.doorintrusion.DoorInstrusionDetailActivity
import mqtt.MQTTService
import noti.MQTTNotiHandler
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended
import org.eclipse.paho.client.mqttv3.MqttMessage
import repository.Repository

class DoorManagementActivity : AppCompatActivity() {
    private lateinit var toolbar: Toolbar

    private lateinit var binding : ActivityDoorManagementBinding
    private lateinit var adapter : DoorRecyclerAdapter
    private lateinit var notiHandler: MQTTNotiHandler
    // GSON CONVERTER
    val gson = Gson()
    private lateinit var doorViewModel : DoorViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_door_management)
        setupToolBar()
        doorViewModel = ViewModelProvider(this).get(DoorViewModel::class.java)

        // Recycler View Setup
        val layoutManager = GridLayoutManager(this,2)
        doorViewModel.getDoors().observe(this, Observer { doorList->
            Log.d("DOOR_ADAPTER:","DOORLIST: ${doorList.toString()}")
            adapter = DoorRecyclerAdapter(doorList, DoorClickListener { door ->

                navigateToDoorDetail(door.id!!)
            }, ButtonClickListener {door->
                Log.d("DOOR_BUTTON_CLICKED:"," ${door.id} at state: ${door.state}")
                doorViewModel.changeDoorState(id = door.id!!)
            })
            binding.doorRecyclerView.adapter = adapter
            binding.doorRecyclerView.layoutManager = layoutManager
        })
        // Batch Commmand Buttons
        binding.closeAllButton.setOnClickListener {
            doorViewModel.commandAllDoor(CLOSE_ALL_DOOR)
        }

        binding.openAllButton.setOnClickListener {
            doorViewModel.commandAllDoor(OPEN_ALL_DOOR)
        }
        // Register the noti channel
        notiHandler = MQTTNotiHandler(this)
    }

    override fun onResume() {
        super.onResume()
        subscribeToTopics(Repository.topicList)
    }
    // Start service and Subscribe to multiples topic on the MQTT broker
    private fun subscribeToTopics(topicList: ArrayList<String>){
        Repository.mqttService.setCallBack(object : MqttCallbackExtended {
            override fun connectComplete(reconnect: Boolean, serverURI: String) {}
            override fun connectionLost(cause: Throwable) {}

            @Throws(Exception::class)
            override fun messageArrived(topic: String, message: MqttMessage) {
                notiHandler.handleMessage(message)
                Log.d("DOOR_GENERAL_CLIENT:", "Received the MQTTmessage as $message")
                val magneticDevice: DoorDevice = gson.fromJson(message.toString(),DoorDevice::class.java)
                if(magneticDevice.name == "MAGNETIC"){
                    // TODO: ALWAYS 1 DUE TO HAVING ONLY ONE DOOR. SERVER SHOULD FIX SO WE DONT HARDCODE
                    doorViewModel.updateDoorWithMQTT(doorID = magneticDevice.id
                        ,state = magneticDevice.data)
                }

            }

            override fun deliveryComplete(token: IMqttDeliveryToken) {}
        })
    }

    // Set up the behavior when the toolbar back arrow is pressed Hàm này để nhấn cái mũi tên góc trái trên màn hình n
    // ó back về ấy copy xong rồi thì nhấn phím 1 cái nha
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }

    // Put the extra to the bundle and display the clicked door detail
    private fun navigateToDoorDetail(doorID : String){
        val intent = Intent(this, DoorInstrusionDetailActivity::class.java)
        intent.putExtra("id",doorID)
        intent.putExtra("role",Repository.getCurrentUser().type)
        startActivity(intent)
    }
    private fun setupToolBar(){
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val actionbar: ActionBar? = supportActionBar
        actionbar?.setDisplayHomeAsUpEnabled(true)
        actionbar?.setDisplayShowHomeEnabled(true)

        // Change the toolbar color
        binding.toolbar.setBackgroundColor(getColor(R.color.door_management_color_theme))
        Log.d("TOOLBAR_COLOR:","${getColor(R.color.teal_700)}")
    }
}
