package door.doorintrusion

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dataclasses.Intrusion
import repository.Repository

class IntrusionViewModel : ViewModel() {
    private lateinit var intrusionLiveData : MutableLiveData<ArrayList<Intrusion>>
    fun getAllIntrusion() = Repository.getIntrusionLiveData()
    fun getIntrusionDoorID(id : String) = Repository.getSpecificDoorIntrusion(id)
    fun getIntrusionDoorIDinInterval(id: String, from: String, to: String) = Repository.getDoorIntrusionWithIDinInterval(id,from,to)
}