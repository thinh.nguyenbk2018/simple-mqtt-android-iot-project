package door.doorintrusion

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.databinding.DoorIntrusionItemBinding
import com.example.myapplication.databinding.DoorStateItemBinding
import dataclasses.Intrusion
import door.DOOR_CLOSE_STRING
import door.DoorClickListener
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class DoorIntrusionDetailRecyclerAdapter(private val intrusionList : ArrayList<Intrusion>
                                            )
    : RecyclerView.Adapter<DoorIntrusionDetailRecyclerAdapter.IntrusionItemViewHolder>(){
    class IntrusionItemViewHolder private constructor(private val binding : DoorIntrusionItemBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(item: Intrusion){
            Log.d("INTRUSION_ADAPTER:","Binding Item: door ${item.doorID}  with Time: ${item.time} converted to: ${item.time?.let {
                getDateTimeFromEpocLongOfSeconds(
                    it
                )
            }}")
            item.time = item.time?.let { getDateTimeFromEpocLongOfSeconds(it) }
            binding.intrusionItem = item

        }
        companion object {
            fun from(parent: ViewGroup) : IntrusionItemViewHolder{
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = DoorIntrusionItemBinding.inflate(layoutInflater,parent, false)
                return IntrusionItemViewHolder(binding)
            }
        }
        private fun getDateTimeFromEpocLongOfSeconds(epoc: String): String? {
            return try {
                val netDate = Date(epoc.toLong())
                netDate.toString()
            } catch (e: Exception) {
                e.toString()
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DoorIntrusionDetailRecyclerAdapter.IntrusionItemViewHolder {
        return IntrusionItemViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: DoorIntrusionDetailRecyclerAdapter.IntrusionItemViewHolder, position: Int) {
        holder.bind(intrusionList[position])
    }

    override fun getItemCount(): Int {
        Log.d("DOOR_ADAPTER:","DOORLIST: ${intrusionList.toString()}")
        return intrusionList.size
    }


}