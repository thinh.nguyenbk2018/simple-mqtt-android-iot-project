package door.doorintrusion

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.R
import com.example.myapplication.databinding.ActivityDoorIntrusionDetailBinding
import noti.MQTTNotiHandler
import repository.Repository

class DoorInstrusionDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDoorIntrusionDetailBinding
    private lateinit var viewModel: IntrusionViewModel
    private lateinit var toolbar: Toolbar
    private lateinit var notiHandler: MQTTNotiHandler
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_door_intrusion_detail)
        setupToolBar()
        viewModel = ViewModelProvider(this).get(IntrusionViewModel::class.java)

        // Get id from bundle
        val doorID: String = intent.extras!!.getString("id", "1")
        if(Repository.getCurrentUser().type == "security"){
            Toast.makeText(this,"Bảo vệ quèn không có quyền xem log của chúng tôi. \ud83d\udeab ", Toast.LENGTH_SHORT).show()
        }
        else {
            val layoutManager = LinearLayoutManager(this)
            // Setup Recycler View
            binding.doorIntrusionRecyclerView.adapter
            binding.doorIntrusionRecyclerView.layoutManager = layoutManager
            viewModel.getIntrusionDoorID(doorID).observe(this, Observer { intrusionList ->
                if (intrusionList == null) {
                    binding.doorIntrusionRecyclerView.visibility = View.GONE
                } else {
                    if(intrusionList.isEmpty()) {
                        Log.d(
                            "DOOR_INTRUSION:",
                            "empty door intrusion list"
                        )
                    }
                    else {Log.d(
                        "DOOR_INTRUSION:",
                        "1st door: ${intrusionList[0].doorID }, with Time: ${{ intrusionList[0].doorID }}"
                    )
                    }
                    binding.doorIntrusionRecyclerView.adapter =
                        DoorIntrusionDetailRecyclerAdapter(intrusionList)
                }
            })
        }

        // Register Notification:
        notiHandler = MQTTNotiHandler(this)
        notiHandler.setCallbackAndHandle()
    }

    private fun setupToolBar() {
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val actionbar: ActionBar? = supportActionBar
        actionbar?.setDisplayHomeAsUpEnabled(true)
        actionbar?.setDisplayShowHomeEnabled(true)

        // Change the toolbar color
        binding.toolbar.setBackgroundColor(getColor(R.color.door_intrusion_color_theme))
    }

    // Set up the behavior when the toolbar back arrow is pressed
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }

}