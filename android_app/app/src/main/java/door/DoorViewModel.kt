package door

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dataclasses.Door
import repository.Repository

class DoorViewModel() : ViewModel() {
    private lateinit var doorLiveData : MutableLiveData<ArrayList<Door>>

    fun getDoors(): LiveData<ArrayList<Door>>{
        doorLiveData = Repository.getDoorLivedata()
        return doorLiveData
    }
    fun updateDoorWithMQTT(doorID: String, state :String): LiveData<ArrayList<Door>>{
        val doorList = doorLiveData.value
        val doorAtID = doorList?.find {
            it.id == doorID
        }
        if (doorAtID != null) {
            doorAtID.state =  state
        }
        if (doorAtID != null) {
            Log.d("DOOR_VIEWMODEL_MQTT:","Change state with new DOOR: door ${doorAtID.id} with state ${doorAtID.state}")
        }
        doorLiveData = Repository.updateDoorStateWithMQTT(doorID,door= doorAtID!!)
        return doorLiveData
    }
    fun changeDoorState(id: String) : LiveData<ArrayList<Door>>{
        val doorList = doorLiveData.value
        val doorAtID = doorList?.find {
            it.id == id
        }
        if (doorAtID != null) {
            doorAtID.state =  if (doorAtID.state == "1") "0" else "1"
        }
        if (doorAtID != null) {
            Log.d("DOOR_VIEWMODEL:","Change state with new DOOR: ${doorAtID.state}")
        }
        doorLiveData = Repository.changeDoorState(doorID = id, newDoor = doorAtID!!)
        return doorLiveData
    }

    fun commandAllDoor(command: String) = Repository.commandAllDoor(command)
}