 package login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.example.myapplication.R
import com.example.myapplication.databinding.ActivityRegisterBinding
import dataclasses.CREATE_ACCOUNT_SUCCESS_MESSAGE
import dataclasses.User
import dataclasses.UserRelatedResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofitapi.RetrofitSingleton

class RegisterActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    private lateinit var binding: ActivityRegisterBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register)
        setupUI()
    }

    private fun setupUI() {
        ArrayAdapter.createFromResource(
            this,
            R.array.account_type_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.accountTypeSpinner.adapter = adapter
        }

        binding.createAccountButton.setOnClickListener {
            navigateToLoginWithInfo()
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        TODO("Not yet implemented")
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

    fun onTextViewClick(view: View) {
        when (view.id) {
            R.id.register_login_text -> {
                navigateToLogin()
            }
        }
    }

    private fun navigateToLogin() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

    private fun navigateToLoginWithInfo() {
        val spinner = binding.accountTypeSpinner
        Log.d(
            "SPINNER:",
            "selectedItem:${spinner.selectedItem}, selectedID:${spinner.selectedItemId}, selectedPosition: ${spinner.selectedItemPosition}"
        )
        if (validateUserChoice()) {
            val newUser = User(userName = binding.userAccountEditText.text.toString(),
                passWord = binding.registerUserPasswordEditText.text.toString(),
                Type = if(binding.accountTypeSpinner.selectedItemPosition == 0) "manager" else "security"
            )
            Log.d("POSTED USER:", "${newUser.username} ${newUser.password} ${newUser.type} ")
            val call = RetrofitSingleton.retrofitUserAPI.createUser(user = newUser)
            call.enqueue(object: Callback<UserRelatedResponse> {

//                override fun onResponse(call: Call<User>, response: Response<User>) {
//                    if(!response.isSuccessful){
//                        Log.d("CREATE_USER:","Response went fine, But not SUCESSFUL with Code: ${response.code()}- ThinhDepTrai")
//                    }
//                    // Bundle the logging information
//                    val intent = Intent(this@RegisterActivity, LoginActivity::class.java)
//                    intent.putExtra("username", binding.userAccountEditText.text.toString())
//                    intent.putExtra("password", binding.registerUserPasswordEditText.text.toString())
//                    Log.d(
//                        "REGISTER_TO_LOGIN:",
//                        "Password passed is: ${binding.registerUserPasswordEditText.text.toString()}"
//                    )
//                    startActivity(intent)
//                }
//
//                override fun onFailure(call: Call<User>, t: Throwable) {
//                    Log.d("CREATE_USER_FAILED:", "${t.message}")
//                    Toast.makeText(this@RegisterActivity, "Failed To Create Account. Try Again Later", Toast.LENGTH_SHORT).show()
//                }

                override fun onResponse(
                    call: Call<UserRelatedResponse>,
                    response: Response<UserRelatedResponse>
                ) {
                    val responseMessage = response.body()?.message ?: "Null Message"
                    if (!response.isSuccessful) {
                        Log.d(
                            "CREATE_USER:",
                            "Response went fine, But not SUCESSFUL with Code: ${response.code()}- ThinhDepTrai"
                        )
                    }
                    // Bundle the logging information
                    else if(response.code() == 200 &&  responseMessage == CREATE_ACCOUNT_SUCCESS_MESSAGE){
                        val intent = Intent(this@RegisterActivity, LoginActivity::class.java)
                        intent.putExtra("username", binding.userAccountEditText.text.toString())
                        intent.putExtra(
                            "password",
                            binding.registerUserPasswordEditText.text.toString()
                        )
                        Log.d(
                            "REGISTER_TO_LOGIN:",
                            "Password passed is: ${binding.registerUserPasswordEditText.text.toString()}"
                        )
                        Toast.makeText(this@RegisterActivity, "Account Created. Now you can Login \\ud83d\\ude01", Toast.LENGTH_SHORT).show()
                        startActivity(intent)
                    }
                }

                override fun onFailure(call: Call<UserRelatedResponse>, t: Throwable) {
                    Log.d("LOGIN_FAILED:", "${t.message}")
                    Toast.makeText(this@RegisterActivity, "Some thing went wrong. ${t.message}", Toast.LENGTH_SHORT).show()
                }

            })

        }
    }


    private fun validateUserChoice(): Boolean {
        Log.d("CREATE_USER_VALIDATION:", "Inside User validation with Password:${binding.registerUserConfirmPasswordEditText.text.toString()}")
        return when {
            TextUtils.isEmpty(binding.userAccountEditText.text.toString()) -> {
                Toast.makeText(this, "Username is required", Toast.LENGTH_SHORT).show()
                false
            }
            TextUtils.isEmpty(binding.registerUserPasswordEditText.text.toString()) -> {
                Toast.makeText(this, "Password is required", Toast.LENGTH_SHORT).show()
                false
            }
            binding.registerUserConfirmPasswordEditText.text.toString() != binding.registerUserPasswordEditText.text.toString()  -> {
                Toast.makeText(
                    this, "Confirm Password does not match. Try again",
                    Toast.LENGTH_SHORT
                ).show()
                false
            }
            else -> true
        }
    }
}
