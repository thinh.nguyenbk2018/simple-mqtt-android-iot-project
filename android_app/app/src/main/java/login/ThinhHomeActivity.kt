package login

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.myapplication.BarChartActivity
import com.example.myapplication.HomeActivity
import com.example.myapplication.R
import com.example.myapplication.databinding.ActivityThinhHomeBinding
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dataclasses.DoorDevice
import door.DoorManagementActivity
import intrusionhistory.IntrusionHistoryActivity
import mqtt.MQTTService
import noti.MQTTNotiHandler
import noti.NotiActivity
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended
import org.eclipse.paho.client.mqttv3.MqttMessage
import stat.StatActivity
import repository.Repository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofitapi.SecurityAPI
import security.ChangeSecurityActivity



class ThinhHomeActivity : AppCompatActivity() {
    private lateinit var binding : ActivityThinhHomeBinding
    private val gson = Gson()
    private lateinit var notiHandler: MQTTNotiHandler
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_thinh_home)
        supportActionBar?.hide()
        val name = if(Repository.getCurrentUser().type == "manager") "Boss" else "Employee"
        binding.welcome.text = "Welcome back, $name"
        // MQTT Service
        subscribeToTopics(Repository.topicList)
        // Noti handler init
        notiHandler = MQTTNotiHandler(this)
    }


    // TODO: REMOVE THE BELOW FUNCTION AFTER TESTING
    private fun publishToAda(topic:String, message: String){
        Repository.mqttService.mqttAndroidClient.publish(topic, MqttMessage(message.toByteArray()))
    }


    // Start listening to MQTT message and reconect/subscribe if needed
    override fun onResume() {
        super.onResume()
        Log.d("HOME_ACT:","RESUMED")
        getStatus()

        /// PUB TESTTER
//        findViewById<Button>(R.id.PUBLIC_BUTTON).setOnClickListener {
//            val message ="{\"id\":\"16\",\"name\":\"INFRARED\",\"data\":\"1\",\"unit\":\"\"}"
//            publishToAda("CSE_BBC1/feeds/bk-iot-infrared",message)
//            Log.d("MESS_PUBLISHED:",message)
//        }


        Repository.mqttService.setCallBack(object : MqttCallbackExtended {
            override fun connectComplete(reconnect: Boolean, serverURI: String) {}
            override fun connectionLost(cause: Throwable?) {
                if (cause != null) {
                    Log.d("HOME_CLIENT:","${cause.message}")
                }
            }
            override fun messageArrived(topic: String, message: MqttMessage) {
                Log.d("HOME_CLIENT:", "Received the meessagea as $message")
                notiHandler.handleMessage(message)
                val ledDevice: DoorDevice = gson.fromJson(message.toString(), DoorDevice::class.java)
                if(ledDevice.name == "LED"){
                    val result = ledDevice.data
                    Log.d("RESULT_HOME_CLIENT:","$result")
                    if (result == "2") {
                        binding.logoutCircle.setImageResource(R.drawable.security_circle_green_home)

                    } else {
                        binding.logoutCircle.setImageResource(R.drawable.security_circle_red_home)
                    }
                    Log.d("MQTT_HOME_CLIENT", result)
                }
            }

            override fun deliveryComplete(token: IMqttDeliveryToken) {}
        })
    }
    // Stop listening to MQTT message and disconect from the broker
    override fun onStop() {
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("HOME_DESTROYDER:","Disconnecting the MQTT Connection - Thinh")
    }

    private fun subscribeToTopics(topicList: ArrayList<String>){
        Repository.mqttService = MQTTService(this,topicList)
        Repository.mqttService.setCallBack(object : MqttCallbackExtended {
            override fun connectComplete(reconnect: Boolean, serverURI: String) {}
            override fun connectionLost(cause: Throwable?) {
                if (cause != null) {
                    Log.d("HOME_CLIENT:","${cause.message}")
                }
            }

            override fun messageArrived(topic: String, message: MqttMessage) {
                Log.d("HOME_CLIENT:", "Received the meessagea as $message")
                val ledDevice: DoorDevice = gson.fromJson(message.toString(), DoorDevice::class.java)
                if(ledDevice.name == "LED"){
                    val result = ledDevice.data
                    Log.d("RESULT_HOME_CLIENT:","$result")
                    if (result == "2") {
                        binding.logoutCircle.setImageResource(R.drawable.security_circle_green_home)
                    } else {
                        binding.logoutCircle.setImageResource(R.drawable.security_circle_red_home)

                    }
                    Log.d("MQTT_HOME_CLIENT", result)
                }
            }

            override fun deliveryComplete(token: IMqttDeliveryToken) {}
        })
    }

    fun thinhClickHandler(view: View) {
        when(view.id){
            R.id.door_image_view, R.id.door_image, R.id.door_text_view ->{
                navigateToDoorManagement()
            }
            R.id.intrusion_image, R.id.intrusion_image_view,R.id.intrusion_text_view ->{
                navigateToIntrusionHistory()
            }

            R.id.change_secu_box, R.id.change_secu_text_view,R.id.change_secu_image_view->{
                Log.d("CHANGE_SECU:","NAVIGATE TO CHANGE SECURITY")
                navigateToChangeSecurityLevel()
            }
            R.id.logout_circle,R.id.logout_text_view ->{
                signOut()
            }
            // TODO: THIN's PART IS NOT DONE
//            R.id.entrace_stat_text_view,R.id.entrance_stat_box,R.id.entrance_stat_image_view->{
//                Log.d("ENTRANCE_STAT:","NAVIGATE TO ENTRANCE")
//                Toast.makeText(this, "Không vào đây được. Duy Thìn Chưa làm xong. Có gì chửi nó \ud83d\ude31", Toast.LENGTH_SHORT).show()
////                navigateToEntranceStat()
//            }
            // TODO: REMOVE THE BELOW LAMBDA WHEN DONE TESTING GET WITH TOKEN
            R.id.imageView3 -> {
                Repository.getWithToken(Repository.getUserToken())
            }
        }

    }


    //TODO: POST THE USED TOKEN TO SERVER FOR INVALIDATING THIS USER AND THIS TKEN
    private fun signOut(){
        Log.d("HOME_CLIENT:","SIGNING OUT")
        val intent = Intent(this, LoginActivity::class.java)
        intent.putExtra("finish", true)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP // To clean up all activities
        Repository.mqttService.mqttAndroidClient.unregisterResources()
        Repository.mqttService.mqttAndroidClient.disconnect()
        startActivity(intent)
        finish()
    }
    // private fun navigateToEntranceStat(){
    //     val intent = Intent(this, StatActivity:: class.java)
    //     startActivity(intent)
    // }
    private fun navigateToDoorManagement() {
        val intent = Intent(this, DoorManagementActivity::class.java)
        startActivity(intent)
    }

    private fun navigateToIntrusionHistory() {
        val intent = Intent(this, IntrusionHistoryActivity::class.java)
        startActivity(intent)
    }
    fun duyThinClickHandler(view: View){
        when(view.id){
            R.id.entrance_stat_box, R.id.entrance_stat_image_view, R.id.entrace_stat_text_view -> {
                navigateToStatisticScreen()
            }
            R.id.imageView7 -> {
                navigateToNotification()
            }
         }
    }
    private fun navigateToStatisticScreen(){
        val intent = Intent(this, StatActivity::class.java)
        startActivity(intent)
    }
    private fun navigateToNotification() {
        val intent = Intent(this, NotiActivity::class.java)
        startActivity(intent)
    }
    private fun navigateToChangeSecurityLevel() {
        val intent = Intent(this, ChangeSecurityActivity:: class.java)
        startActivity(intent)
    }

    // Get security state first time
    private fun getStatus() {
//        val statusSecurity = findViewById(R.id.status_security_home)
        val gson = GsonBuilder()
            .setLenient()
            .create()
        val retrofit = Retrofit.Builder()
            .baseUrl("http://52.163.201.3:3000")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        fun TextView.setTextColor(color: Long) = this.setTextColor(color.toInt())

        val service = retrofit.create(SecurityAPI::class.java)
        val call = service.getSecurityLevel()

        var result: String = ""
        call.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                result = response.body().toString()
                if (result == "0") {
                    binding.logoutCircle.setImageResource(R.drawable.security_circle_green_home)
//                    statusSecurity.text = "OFF"
//                    statusSecurity.setTextColor(Color.parseColor("#233D0A"))
                } else {
                    binding.logoutCircle.setImageResource(R.drawable.security_circle_red_home)
//                    statusSecurity.text = "ON"
//                    statusSecurity.setTextColor(Color.parseColor("#550F0F"))
                }
                Log.d("GET_SECURITY_LEVEL_HOME", result)
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.d("GET_SECURITY_LEVEL_FAILED_HOME", "${t.message}")
            }
        })
    }
}