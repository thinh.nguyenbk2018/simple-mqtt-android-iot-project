package login

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import apitest.TestWeatherAPIActivity
import com.example.myapplication.R
import com.example.myapplication.databinding.ActivityLoginBinding
import dataclasses.MessageWithTokenAndTopics
import dataclasses.User
import door.DoorManagementActivity
import intrusionhistory.IntrusionHistoryActivity
import mqtt.MQTTService
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended
import org.eclipse.paho.client.mqttv3.MqttMessage
import repository.Repository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofitapi.RetrofitSingleton
import stat.StatActivity

class LoginActivity : AppCompatActivity() {
    private lateinit var logginButon: Button
    private lateinit var binding: ActivityLoginBinding

    // TODO: BELOW BUTTONS ARE FOR TESTING PURPOSE EARLY
    private lateinit var doorTestButton: Button
    private lateinit var statButton: Button
    private lateinit var intrusionHistoryTestButton: Button

    // MQTT Service
    private lateinit var mqttService: MQTTService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)

        binding.loginButton.setOnClickListener {
            authenticateAndLogin()
        }

        supportActionBar?.hide()

    }


    override fun onStart() {
        super.onStart()
        if(intent.extras != null){
            val passedUsername = intent.extras!!.getString("username")
            val passedPassword = intent.extras!!.getString("password")
            binding.userAccountEditText.setText(passedUsername)
            binding.userPasswordEditText.setText(passedPassword)
        }
    }
    private fun authenticateAndLogin() {
        if (validateUserChoice()) {
            val user = User(
                userName = binding.userAccountEditText.text.toString(),
                passWord = binding.userPasswordEditText.text.toString(),
                Type = "nothing relevant"
            )
            val call = RetrofitSingleton.retrofitUserAPI.authenticateUser(user)
            call.enqueue(object : Callback<MessageWithTokenAndTopics> {
                override fun onResponse(
                    call: Call<MessageWithTokenAndTopics>,
                    response: Response<MessageWithTokenAndTopics>
                ) {
                    val message = response.body()?.message
                    if (response.code() == 200 && response.body()!!.message.contains("successfully")) {
                        val resBody = response.body()!!
                        user.type = resBody.message.substringAfter("type: ")
                        Repository.setCurrentUser(user)
                        Repository.setUserToken(resBody.accessToken)
                        Repository.topicList = resBody.mqtt_connection_string
//                        subscribeToTopics(Repository.topicList)
                        navigateToHome()
                    }
                    else{
                        Toast.makeText(this@LoginActivity, "$message", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<MessageWithTokenAndTopics>, t: Throwable) {
                    Log.d("LOGIN_FAILED:", "${t.message}")
                    Toast.makeText(this@LoginActivity, "Some thign went wrong. ${t.message}", Toast.LENGTH_SHORT).show()
                }

            })
        }
    }

    // Start service and Subscribe to multiples topic on the MQTT broker
    // TODO: IMPLMENT THE CALLBACK: WHAT TO DO WHEN RECEIVING NEW MESSAGE
    private fun subscribeToTopics(topicList: ArrayList<String>){
        Repository.mqttService = MQTTService(this,topicList)
        Repository.mqttService.setCallBack(object : MqttCallbackExtended {
            override fun connectComplete(reconnect: Boolean, serverURI: String) {}
            override fun connectionLost(cause: Throwable) {}

            @Throws(Exception::class)
            override fun messageArrived(topic: String, message: MqttMessage) {
                val data_to_microbit = message.toString()
                Log.d("ANDROID_APP:", "Received the meessagea as $message")
            }

            override fun deliveryComplete(token: IMqttDeliveryToken) {}
        })
    }


    private fun navigateToHome() {
        val intent = Intent(this, ThinhHomeActivity::class.java)
        startActivity(intent)
    }

    fun onTextViewClick(view: View) {
        when (view.id) {
            R.id.sign_up_text -> {
                navigateToRegister()
            }
        }
    }

    private fun navigateToRegister() {
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
    }

    private fun validateUserChoice(): Boolean {
        Log.d(
            "CREATE_USER_VALIDATION:",
            "Inside User validation with Password:${binding.userPasswordEditText.text.toString()}"
        )
        return when {
            TextUtils.isEmpty(binding.userAccountEditText.text.toString()) -> {
                Toast.makeText(this, "Username is required", Toast.LENGTH_SHORT).show()
                false
            }
            TextUtils.isEmpty(binding.userPasswordEditText.text.toString()) -> {
                Toast.makeText(this, "Password is required", Toast.LENGTH_SHORT).show()
                false
            }
            else -> true
        }
    }


    // TODO: BELOW FUNCTIONS ARE FOR TESTING PURPOSE EARLY
    private fun navigateToStat() {
        val intent = Intent(this, StatActivity::class.java)
        startActivity(intent)
    }

    private fun navigateToWeatherAPItest() {
        val intent = Intent(this, TestWeatherAPIActivity::class.java)
        startActivity(intent)
    }

    private fun navigateToDoorManagement() {
        val intent = Intent(this, DoorManagementActivity::class.java)
        startActivity(intent)
    }

    private fun navigateToIntrusionHistory() {
        val intent = Intent(this, IntrusionHistoryActivity::class.java)
        startActivity(intent)
    }


}





// CODING GRAVEYARD:

