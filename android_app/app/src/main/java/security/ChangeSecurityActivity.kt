package security

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.Ringtone
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.ImageView
import android.widget.RemoteViews
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.Toolbar
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.myapplication.R
import com.example.myapplication.databinding.ActivityDoorManagementBinding
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dataclasses.DoorDevice
import dataclasses.Security
import dataclasses.SecurityCommand
import mqtt.MQTTService
import noti.MQTTNotiHandler
import noti.NotiActivity
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended
import org.eclipse.paho.client.mqttv3.MqttMessage
import repository.Repository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofitapi.SecurityAPI

class ChangeSecurityActivity : AppCompatActivity() {

    private lateinit var toolbar: Toolbar

    private lateinit var statusSecurity: TextView
    private lateinit var txtSecurity: TextView
    private lateinit var circleSecurity: ImageView
    private lateinit var bottomSecurity: ImageView
    private lateinit var notiHandler: MQTTNotiHandler
    private val gson = Gson()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_security)
        setUpToolBar()
        getSecurityLevel()
        statusSecurity = findViewById(R.id.statusSecurity)
        circleSecurity = findViewById(R.id.circle_security_level)
        bottomSecurity = findViewById(R.id.bottom_security_level)
        txtSecurity = findViewById(R.id.txt_security)

        //Subscribe to MQTT Topic
        subscribeToTopics(Repository.topicList)
        notiHandler = MQTTNotiHandler(this)
        statusSecurity.setOnClickListener() {
            setSecurityLevel()
        }


    }

    override fun onResume() {
        super.onResume()
        subscribeToTopics(Repository.topicList)
    }

    private fun subscribeToTopics(topicList: ArrayList<String>) {
        Repository.mqttService.setCallBack(object : MqttCallbackExtended {
            override fun connectComplete(reconnect: Boolean, serverURI: String) {}
            override fun connectionLost(cause: Throwable) {}

            @Throws(Exception::class)
            override fun messageArrived(topic: String, message: MqttMessage) {
                notiHandler.handleMessage(message)
                Log.d("CHANGE_SECURITY_CLIENT:", "Received the meessagea as $message")
                val ledDevice: DoorDevice =
                    gson.fromJson(message.toString(), DoorDevice::class.java)
                if (ledDevice.name == "LED") {
                    val result = ledDevice.data
                    if (result == "2") {
                        statusSecurity.text = "OFF"
                        statusSecurity.setTextColor(Color.parseColor("#233D0A"))
                        txtSecurity.setTextColor(Color.parseColor("#233D0A"))
                        circleSecurity.setImageResource(R.drawable.ellipse_20)
                        bottomSecurity.setImageResource(R.drawable.vector_60)
                    } else {
                        statusSecurity.text = "ON"
                        statusSecurity.setTextColor(Color.parseColor("#550F0F"))
                        txtSecurity.setTextColor(Color.parseColor("#550F0F"))
                        circleSecurity.setImageResource(R.drawable.ellipse_7)
                        bottomSecurity.setImageResource(R.drawable.vector_6)
                    }
                    Log.d("CHANGE_SECURITY_CLIENT", result)
                }
            }

            override fun deliveryComplete(token: IMqttDeliveryToken) {}
        })
    }

    private fun setSecurityLevel() {
        val gson = GsonBuilder()
            .setLenient()
            .create()
        val retrofit = Retrofit.Builder()
            .baseUrl("http://52.163.201.3:3000")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        val service = retrofit.create(SecurityAPI::class.java)
        val nowStatus = getSecurityLevel()

        statusSecurity.text = "Waiting..."
        statusSecurity.setTextColor(Color.parseColor("#3366CC"))
        Log.d("Button change security", "disable");
        statusSecurity.isClickable = false;

        val value: String = if (nowStatus == "0") {
            "1"
        } else {
            "0"
        }
        val command = SecurityCommand(value)

        val call = service.setSecurityLevel(Repository.getUserToken(), command)

        call.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                if (!response.isSuccessful) {
                    Log.d("SET_SECURITY_LEVEL", "$response")
                } else {
                    Log.d("result", "${response.body()}")
                    statusSecurity.isClickable = true;
                    Log.d("Button change security", "enable");
                    getSecurityLevel()
                }
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.d("SET_SECURITY_LEVEL_FAILED", "${t.message}")
            }

        })

    }

    var result: String = "";
    private fun getSecurityLevel(): String {
        val gson = GsonBuilder()
            .setLenient()
            .create()
        val retrofit = Retrofit.Builder()
            .baseUrl("http://52.163.201.3:3000")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        fun TextView.setTextColor(color: Long) = this.setTextColor(color.toInt())

        val service = retrofit.create(SecurityAPI::class.java)
        val call = service.getSecurityLevel()
        call.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                result = response.body().toString()
                if (result == "0") {
                    statusSecurity.text = "OFF"
                    statusSecurity.setTextColor(Color.parseColor("#233D0A"))
                    txtSecurity.setTextColor(Color.parseColor("#233D0A"))
                    circleSecurity.setImageResource(R.drawable.ellipse_20)
                    bottomSecurity.setImageResource(R.drawable.vector_60)
                } else {
                    statusSecurity.text = "ON"
                    statusSecurity.setTextColor(Color.parseColor("#550F0F"))
                    txtSecurity.setTextColor(Color.parseColor("#550F0F"))
                    circleSecurity.setImageResource(R.drawable.ellipse_7)
                    bottomSecurity.setImageResource(R.drawable.vector_6)
                }
                Log.d("GET_SECURITY_LEVEL", result)
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.d("GET_SECURITY_LEVEL_FAILED", "${t.message}")
            }
        })
        return result
    }

    // Set up the behavior when the toolbar back arrow is pressed
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }


    private fun setUpToolBar() {
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val actionbar: ActionBar? = supportActionBar
        actionbar?.setDisplayHomeAsUpEnabled(true)
        actionbar?.setDisplayShowHomeEnabled(true)

        toolbar.setBackgroundColor(getColor(R.color.change_security_theme))
    }


}