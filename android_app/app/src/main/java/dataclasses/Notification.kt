package dataclasses

import com.google.gson.annotations.SerializedName

class Notification {
    constructor(iid: String, itime:String, idoorID:String){
        id = iid
        time = itime
        doorId = idoorID
    }
    @SerializedName("_id")
    var id : String? = null

    @SerializedName("time")
    var time : String? = null

    @SerializedName("doorId")
    var doorId : String? = null
}