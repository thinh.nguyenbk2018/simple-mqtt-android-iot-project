package dataclasses

import com.google.gson.annotations.SerializedName

class User{
    @SerializedName("username")
    var username : String? = null
    @SerializedName("password")
    var password : String? = null
    @SerializedName("type")
    var type : String? = null

    constructor(userName: String,passWord: String,Type:String){
        username = userName
        password = passWord
        type = Type   // Can only be: manager/security
    }
}