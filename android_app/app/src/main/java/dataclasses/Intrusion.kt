package dataclasses

import com.google.gson.annotations.SerializedName

class Intrusion {
    @SerializedName("doorId")
    var doorID : String? = null
    @SerializedName("time")
    var time : String? = null
}