package dataclasses

import com.google.gson.annotations.SerializedName

 data class Security(
    var value: String
)