package dataclasses



import com.google.gson.annotations.SerializedName

class Stat {
    @SerializedName("time")
    var time : String? = null

    @SerializedName("doorID")
    var id : String? = null
}