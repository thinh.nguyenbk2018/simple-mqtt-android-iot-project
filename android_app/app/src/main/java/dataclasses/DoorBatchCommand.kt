package dataclasses

import com.google.gson.annotations.SerializedName
const val CLOSE_ALL_DOOR = "CLOSE_ALL"
const val OPEN_ALL_DOOR = "OPEN_ALL"
data class DoorBatchCommand(var command :String)