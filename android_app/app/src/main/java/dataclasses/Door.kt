package dataclasses

import com.google.gson.annotations.SerializedName

class Door(id: String?, newState: String) {

    @SerializedName("id")
    var id : String? = null
    @SerializedName("state")
    var state : String? = null
}
// Possible device name: MAGNETIC / SERVO
data class DoorDevice(
    @SerializedName("id")
    var id: String,
    @SerializedName("name")
    var name: String,
    @SerializedName("data")
    var data: String,
    @SerializedName("unit")
    var unit: String)