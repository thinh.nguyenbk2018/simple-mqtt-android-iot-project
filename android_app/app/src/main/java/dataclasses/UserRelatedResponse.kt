package dataclasses

//client.connect();
//const clt = client.db("MQTT").collection('account');
//
//router.post("/", async (req, res) => {
//    let username = req.body.username;
//    let password = req.body.password;
//    let type = req.body.type;
//    clt.findOne({username: username}, (err, result) => {
//        if(result) {
//            res.status(400).send({message: "User already exists"});
//        }
//    })
//
//    const salt = await bcrypt.genSalt();
//    const hashedPassword = await bcrypt.hash(password, salt);
//    clt.insertOne({
//            username: username,
//            password: hashedPassword,
//            type : type
//    })
//    res.status(200).send({message: "Success"});
//});
//
//router.post("/auth", async (req, res) => {
//    let username = req.body.username;
//    let password = req.body.password;
//    clt.findOne({
//            username: username
//    }, (err, result) => {
//        console.log(result);
//        if(err) throw err;
//        if(result == null) {
//            return res.status(400).send({message: "Cannot find user"});
//        }
//
//        if(bcrypt.compareSync(password, result.password)) {
//            res.status(200).send({message: "Success"});
//        } else {
//            res.status(400).send({message: "Invalid Password"});
//        }
//    })
//});

const val CREATE_ACCOUNT_SUCCESS_MESSAGE = "Success"
const val CREATE_ACCOUNT_USER_ALREADY_EXIST_MESSAGE = "User already exists"

const val AUTHENTICATE_ACCOUNT_SUCCESS_MESSAGE = "Success"
const val AUTHENTICATE_ACCOUNT_USER_NOT_EXIST_MESSAGE = "Cannot find user"
const val AUTHENTICATE_ACCOUNT_WRONG_PASSWORD_MESSAGE = "Invalid password"
data class UserRelatedResponse(val message :String)

data class MessageWithToken(val message: String, val accessToken: String)
// Third argument is the list of topics to subscribe to for the MQTT Client.
data class MessageWithTokenAndTopics(val message: String, val accessToken: String, val mqtt_connection_string: ArrayList<String>)