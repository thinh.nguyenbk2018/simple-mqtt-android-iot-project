package com.example.myapplication

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dataclasses.DoorDevice
import mqtt.MQTTService
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended
import org.eclipse.paho.client.mqttv3.MqttMessage
import repository.Repository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofitapi.SecurityAPI
import security.ChangeSecurityActivity
import stat.StatActivity

class HomeActivity : AppCompatActivity() {

    private lateinit var changeSecurityButton: ImageView
    private lateinit var changeSecurityTextView: TextView
    private lateinit var intrusionHistoryButton: ImageView
    private lateinit var intrusionHistoryTextView: TextView
    private lateinit var doorStatusButton: ImageView
    private lateinit var doorStatusTextView: TextView
    private lateinit var entranStatButton: ImageView
    private lateinit var entranStatTextView: TextView
    private lateinit var statusSecurity: TextView
    private lateinit var notice: ImageView
    private lateinit var information_user: ImageView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        getStatus()

        entranStatButton = findViewById(R.id.stat_btn)
        entranStatTextView = findViewById(R.id.stat_tv)
        entranStatButton.setOnClickListener() {
            val intent = Intent(this, ChangeSecurityActivity::class.java)
            startActivity(intent)
        }

        entranStatTextView.setOnClickListener() {
            val intent = Intent(this, ChangeSecurityActivity::class.java)
            startActivity(intent)
        }


        doorStatusButton = findViewById(R.id.door_status_btn)
        doorStatusTextView = findViewById(R.id.door_status_tv)
        doorStatusButton.setOnClickListener() {
            val intent = Intent(this, ChangeSecurityActivity::class.java)
            startActivity(intent)
        }

        doorStatusTextView.setOnClickListener() {
            val intent = Intent(this, ChangeSecurityActivity::class.java)
            startActivity(intent)
        }


        intrusionHistoryButton = findViewById(R.id.history_intrusion_btn)
        intrusionHistoryTextView = findViewById(R.id.history_intrusion_tv)
        intrusionHistoryTextView.setOnClickListener() {
            val intent = Intent(this, ChangeSecurityActivity::class.java)
            startActivity(intent)
        }

        intrusionHistoryButton.setOnClickListener() {
            val intent = Intent(this, ChangeSecurityActivity::class.java)
            startActivity(intent)
        }

        changeSecurityButton = findViewById(R.id.change_security_btn)
        changeSecurityTextView = findViewById(R.id.change_security_tv)
        changeSecurityButton.setOnClickListener() {
            val intent = Intent(this, ChangeSecurityActivity::class.java)
            startActivity(intent)
        }

        changeSecurityTextView.setOnClickListener() {
            val intent = Intent(this, ChangeSecurityActivity::class.java)
            startActivity(intent)
        }

        notice = findViewById(R.id.notice_id)
        notice.setOnClickListener() {
            val intent = Intent(this, ChangeSecurityActivity::class.java)
            startActivity(intent)
        }

        information_user = findViewById(R.id.information_id)
        information_user.setOnClickListener() {
            val intent = Intent(this, ChangeSecurityActivity::class.java)
            startActivity(intent)
        }

        statusSecurity = findViewById(R.id.status_security_home)
        statusSecurity.setOnClickListener() {
            getStatus()
        }
    }

    private fun getStatus() {
        statusSecurity = findViewById(R.id.status_security_home)
        val gson = GsonBuilder()
            .setLenient()
            .create()
        val retrofit = Retrofit.Builder()
            .baseUrl("http://20.191.152.52:3000")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        fun TextView.setTextColor(color: Long) = this.setTextColor(color.toInt())

        val service = retrofit.create(SecurityAPI::class.java)
        val call = service.getSecurityLevel()

        var result: String = ""
        call.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                result = response.body().toString()
                if (result == "0") {
                    statusSecurity.text = "OFF"
                    statusSecurity.setTextColor(Color.parseColor("#233D0A"))
                } else {
                    statusSecurity.text = "ON"
                    statusSecurity.setTextColor(Color.parseColor("#550F0F"))
                }
                Log.d("GET_SECURITY_LEVEL_HOME", result)
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.d("GET_SECURITY_LEVEL_FAILED_HOME", "${t.message}")
            }
        })
    }
}