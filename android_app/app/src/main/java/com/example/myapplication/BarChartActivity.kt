package com.example.myapplication

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.data.*

class BarChartActivity : AppCompatActivity() {


    lateinit var barList:ArrayList<BarEntry>
    lateinit var barDataSet : BarDataSet
    lateinit var barData : BarData


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bar_chart)
        setData()

    }
    fun setData(){
        barList = ArrayList()
        barList.add(BarEntry(10f, 5f))
        barList.add(BarEntry(30f, 10f))
        barList.add(BarEntry(70f, 40f))
        barList.add(BarEntry(40f, 70f))
        barList.add(BarEntry(10f, 20f))
        barList.add(BarEntry(20f, 30f))
        barList.add(BarEntry(50f, 50f))
        barList.add(BarEntry(30f, 100f))

        barDataSet = BarDataSet(barList, "Population")
        barData = BarData(barDataSet)

        val barChart = findViewById<BarChart>(R.id.barChart)

        barChart.data = barData

        barDataSet.setColors(ColorTemplate.JOYFUL_COLORS, 250)
        barDataSet.valueTextColor= Color.BLACK
        barDataSet.valueTextSize = 15f

    }
}