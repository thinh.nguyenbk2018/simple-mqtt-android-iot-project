package apitest

import retrofit2.http.GET
import retrofit2.Call
import retrofit2.http.Query

interface IFoodAPI {
    @GET("button")
    fun getString() : Call<String>

//    @GET("categories.php")
//    fun getCategories() : Call<Locale.Category>
//

}

interface WeatherService{
    @GET("data/2.5/weather?")
    fun getCurrentWeatherData(@Query("lat") lat: String, @Query("lon") lon :String, @Query("APPID") app_id : String) : Call<WeatherResponse>
}

interface TestStringService{
    @GET("button")
    fun getString() : Call<String>
}

