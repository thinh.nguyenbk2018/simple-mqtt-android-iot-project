package apitest

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Button
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.myapplication.R
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class TestWeatherAPIActivity : AppCompatActivity() {
    private lateinit var fetchButton : Button
    private lateinit var resultTextView: TextView
    private lateinit var toolbar: Toolbar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_weather_a_p_i)
        initializeUI()
    }
    private fun getCurrentData(){
        val gson = GsonBuilder()
            .setLenient()
            .create()
        val retrofit = Retrofit.Builder()
            .baseUrl(BaseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        val service = retrofit.create(TestStringService::class.java)
        val call = service.getString()
        call.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                Log.d("BUTTON_GET:", "${response}")
                val result = response.body()
                resultTextView.text = result.toString()
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.d("BUTTON_GET", "${t.message}")
            }
        })
//        call.enqueue(object : Callback<String>{
//            override fun onResponse(
//                call: Call<String>,
//                response: Response<WeatherResponse>
//            ) {
//                if(response.code() == 200){
//                    Log.d("WEATHER_RESULT:","${response.body()}")
//                    val weatherResponse = response.body()
//
//                    val stringBuilder = "Country: " +
//                            weatherResponse?.sys!!.country +
//                            "\n" +
//                            "Temperature: " +
//                            weatherResponse.main!!.temp +
//                            "\n" +
//                            "Temperature(Min): " +
//                            weatherResponse.main!!.temp_min +
//                            "\n" +
//                            "Temperature(Max): " +
//                            weatherResponse.main!!.temp_max +
//                            "\n" +
//                            "Humidity: " +
//                            weatherResponse.main!!.humidity +
//                            "\n" +
//                            "Pressure: " +
//                            weatherResponse.main!!.pressure
//                    resultTextView.text = stringBuilder
//                }
//            }
//
//            override fun onFailure(call: Call<WeatherResponse>, t: Throwable) {
//                Log.d("WEATHER_RESULT","${t.message}")
//                resultTextView.text = t.message
//            }
//        })
    }
    private fun initializeUI(){
        // Toolbar setup
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        val actionbar: ActionBar? = supportActionBar
        actionbar?.setDisplayHomeAsUpEnabled(true)
        actionbar?.setDisplayShowHomeEnabled(true)
        // Change the toolbar color
        toolbar.setBackgroundColor(getColor(R.color.teal_700))
        Log.d("TOOLBAR_COLOR:","${getColor(R.color.teal_700)}")


        fetchButton = findViewById(R.id.weather_fecth_button)
        resultTextView = findViewById(R.id.weather_result_text)
        fetchButton.setOnClickListener {
            getCurrentData()
        }
    }


    // Set up the behavior when the toolbar back arrow is pressed
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }

    companion object{
        var BaseUrl = "http://20.191.152.52:3000"
        var AppId = "2e65127e909e178d0af311a81f39948c"
        var lat = "14"
        var lon = "108"
    }
}