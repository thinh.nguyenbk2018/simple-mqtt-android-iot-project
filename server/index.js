const express = require("express");
const routes = require("./routers/index");
const cors = require("cors");
const app = express();
require("dotenv").config();
const port = 9999;
console.log(process.env.PORT);
var corsOptions = {
  origin: "http://localhost:8081",
};
app.use(cors(corsOptions));
//built-in middleware
app.use(express.json());
app.get("/", (req, res) => {
  res.send("Hello World");
});
app.use("/", routes);
app.listen(port, async () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
