const express = require("express");
const router = express.Router();
const pub = require("../broker/pub");
const { MongoClient } = require("mongodb");
const url = "mongodb://localhost:27017";
const client = new MongoClient(url, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
client.connect();
const clt = client.db("MQTT").collection("door");

router.get("/", (req, res) => {
  clt
    .find()
    .sort({ id: 1 })
    .toArray((err, result) => {
      if (err) throw err;
      result.forEach((obj) => delete obj["_id"]);
      res.send(result);
    });
});
router.patch("/all", async (req, res) => {
  clt.find().count({}, (err, result) => {
    if (result > 0) {
      let data = req.body.command == "CLOSE_ALL" ? "0" : "90";
      var i;
      let topic;
      for (i = 1; i <= result; i++) {
        topic = "servo" + i.toString();
        pub(topic, data);
      }
    }
  });
  if (req.body.command == "CLOSE_ALL") {
    clt.updateMany({}, { $set: { state: "0" } });
  } else {
    clt.updateMany({}, { $set: { state: "1" } });
  }
  clt.find().toArray((err, result) => {
    if (err) throw err;
    result.forEach((obj) => delete obj["_id"]);
    res.send(result);
  });

  return;
});

router.patch("/:id", async (req, res) => {
  let data = req.body.state == "0" ? "90" : "0";
  let topic = "servo";
  console.log(topic, data);
  pub(topic, data);
  let query = { id: req.params.id };
  clt.find(query).toArray((err, result) => {
    if (err) throw err;
    result.forEach((obj) => delete obj["_id"]);
    //response
    let obj = {
      id: req.params.id,
      state: req.body.state,
    };
    res.send(obj);
  });
});

module.exports = router;
