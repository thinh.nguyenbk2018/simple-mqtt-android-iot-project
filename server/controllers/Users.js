const config = require("../auth.config");
//const topics = require("../utils/topic");
const jwt = require('jsonwebtoken');
var CryptoJS = require("crypto-js");

const {MongoClient} = require("mongodb");
const url = "mongodb://localhost:27017";
const client = new MongoClient(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});
client.connect();
const User = client.db("MQTT").collection('account');

signup = (req, res) => {
    let username = req.body.username;
    let password = req.body.password;
    let type = req.body.type;
    const hashedPassword = CryptoJS.AES.encrypt(password, 'secret key').toString();
    User.insertOne({
        username: username,
        password: hashedPassword,
        type : type
    });
    res.status(200).send({message: "Success"});
};

signin = (req, res) => {
    let topics = [
        "thanhngodinh/feeds/button",
        "thanhngodinh/feeds/magnetic",
        "thanhngodinh/feeds/infrared",
        "thanhngodinh/feeds/servo",
        "thanhngodinh/feeds/led",
        "thanhngodinh/feeds/speaker"
    ]
    let username = req.body.username;
    let password = req.body.password;
    User.findOne({
        username: username
    }, (err, result) => {
        if(err) throw err;
        if(result == null) {
            return res.status(400).send({message: "Cannot find user"});
        } 
        var bytes  = CryptoJS.AES.decrypt(result.password, 'secret key');
        var originalText = bytes.toString(CryptoJS.enc.Utf8);
        if(password == originalText) {
            let msg = "Authenticated successfully. type: " + result.type
            let user = {name: username};
            const accessToken = jwt.sign(user, config.secret, {
                expiresIn: config.tokenLife
            });
            res.status(200).send({
                message: msg,
                accessToken: accessToken,
                mqtt_connection_string: topics
            });
        } else {
            res.status(400).send({message: "Invalid Password"});
        }
    })
}

module.exports = {
    signup,
    signin
};