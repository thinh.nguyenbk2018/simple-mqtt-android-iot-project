const express = require("express");
const router = express.Router();
const { authJWT } = require("../middlewares");

const { MongoClient } = require("mongodb");
const url = "mongodb://localhost:27017";
const client = new MongoClient(url, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
client.connect();
const clt = client.db("MQTT").collection("intrusion");

router.get("/", authJWT.verifyToken, authJWT.isManager, (req, res) => {
  clt.find().toArray((err, result) => {
    if (err) throw err;
    result.forEach((obj) => delete obj["_id"]);
    res.send(result);
  });
});

router.get("/:id", authJWT.verifyToken, authJWT.isManager, (req, res) => {
  let query = { doorID: req.params.id };
  console.log(query);
  clt.find(query).toArray((err, result) => {
    if (err) throw err;
    result.forEach((obj) => delete obj["_id"]);
    res.send(result);
  });
  // if(Object.keys(req.query).length === 0 && req.query.constructor === Object) {
  //     let query = {doorId: req.params.id}
  //     clt.find(query).toArray((err, result) => {
  //         if (err) throw err;
  //         result.forEach(obj => delete obj["_id"]);
  //         res.send(result);
  //     })
  // }

  // else {
  //     console.log(req.query.from);
  //     let startDate = req.query.from;
  //     let endDate = req.query.to;
  //     clt.find({
  //         time: {
  //             $gte: new Date(startDate).setHours(00,00,00),
  //             $lt: new Date(endDate).setHours(23,59,59)
  //         }
  //     }).toArray((err, result) => {
  //         if (err) throw err;
  //         result.forEach(obj => delete obj["_id"]);
  //         res.send(result);
  //     })
  // }
});

module.exports = router;
