const express = require("express");
const router = express.Router();
const pub = require("../broker/pub");

const {MongoClient} = require("mongodb");
const url = "mongodb://localhost:27017";
const client = new MongoClient(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});
client.connect();
const clt = client.db("MQTT").collection('notification');
var securityLevel = "0";

router.get("/", async (req, res) => {
    res.send(securityLevel);
});

router.put("/", async (req, res) => {
    let data = req.body.value == "1" ? "1" : "2";
    let topic = "led";
    pub(topic, data);
    if(req.body.value == "1") {
        securityLevel = "1";
        newValue = {
            data : "The mall's security was set to ON (high level security)",
            type : "security_change"
        }
    } else {
        securityLevel = "0";
        newValue = {
            data : "The mall's security was set to OFF (safety security)",
            type : "security_change"
        }
    }
    clt.insertOne(newValue, (err, res) => {
        if (err) throw err;
    })
    res.send(securityLevel);
})

module.exports = router;