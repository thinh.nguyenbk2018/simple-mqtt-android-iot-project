const express = require("express");
const router = express.Router();

const {MongoClient} = require("mongodb");
const url = "mongodb://localhost:27017";
const client = new MongoClient(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});
client.connect();
const clt = client.db("MQTT").collection('intrusion');

router.get("/", (req, res) => {
    clt.find().limit(5).toArray((err, result) => {
        if (err) throw err;
        res.send(result);
    });
});

module.exports = router;