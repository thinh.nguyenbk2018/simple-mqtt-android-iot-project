const express = require("express");
const router = express.Router();
//const {authJWT} = require("../middlewares")

const {MongoClient} = require("mongodb");
const url = "mongodb://localhost:27017";
const client = new MongoClient(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});
client.connect();
const clt = client.db("MQTT").collection('entrance');

router.get("/" , (req, res) => {
    clt.find().toArray((err, result) => {
        if (err) throw err;
        result.forEach(obj => delete obj["_id"]);
        
        res.send(result);
    });

});

router.get("/:id", (req, res) => {
    var query = {doorId: req.params.id}
    clt.find(query).toArray((err, result) => {
        if (err) throw err;
        result.forEach(obj => delete obj["_id"]);
        
        res.send(result);
    })
})

module.exports = router;