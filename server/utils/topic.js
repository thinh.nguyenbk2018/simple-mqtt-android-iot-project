let topics = {
    //Button
    button: "thanhngodinh/feeds/button",
    //SERVO
    servo: "thanhngodinh/feeds/servo",
    //Speaker
    speaker: "thanhngodinh/feeds/speaker",
    //Switch
    switch: "thanhngodinh/feeds/magnetic",
    //Led
    led: "thanhngodinh/feeds/led",
    //Infrared Sensor
    sensor: "thanhngodinh/feeds/infrared"
};

module.exports = topics;