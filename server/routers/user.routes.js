const express = require("express");
const router = express.Router();

const {verifySignUp, authJWT} = require("../middlewares")
const userController = require("../controllers/Users")

router.use((req, res, next) => {
    res.header(
        "Access-Control-Allow-Headers",
        "access-token, Origin, Content-Type, Accept"
    );
    next();
});

router.get("/", authJWT.verifyToken, (req, res) => {
    res.send("user")
})

router.post(
    "/",
    verifySignUp.checkDuplicateUsername,
    userController.signup
);

router.post(
    "/auth",
    userController.signin
);

module.exports = router;