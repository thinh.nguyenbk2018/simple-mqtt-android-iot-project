const express = require("express");
const DoorStatus = require("../controllers/DoorStatus");
const Security = require("../controllers/Security");
const IntrusionHistory = require("../controllers/IntrusionHistory");
const EntranceStatistics = require("../controllers/EntranceStatistics");
const Notification = require("../controllers/Notification");
const Users = require("../routers/user.routes");
const router = express.Router();

router.use("/door-status", DoorStatus);
router.use("/security", Security);
router.use("/intrusion-history", IntrusionHistory);
router.use("/entrance-stat", EntranceStatistics);
router.use("/notification", Notification);
router.use("/users", Users);

module.exports = router;