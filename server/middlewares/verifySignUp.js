const {MongoClient} = require("mongodb");
const url = "mongodb://localhost:27017";
const client = new MongoClient(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});
client.connect();
const User = client.db("MQTT").collection('account');

checkDuplicateUsername = (req, res, next) => {
    User.findOne({
        username: req.body.username
    }, (err, user) => {
        if(err) {
            res.status(500).send({ message: err });
            return;
        }
        if (user) {
            res.status(400).send({message: "User already exists"});
            return;
        }
        next();
    })
}

const verifySignUp = {
    checkDuplicateUsername
};
module.exports = verifySignUp;