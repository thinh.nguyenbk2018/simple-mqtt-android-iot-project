const jwt = require("jsonwebtoken");
const config = require("../auth.config")
const {MongoClient} = require("mongodb");
const url = "mongodb://localhost:27017";
const client = new MongoClient(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});
client.connect();
const User = client.db("MQTT").collection('account');

verifyToken = (req, res, next) => {
    let token = req.headers["access-token"];
    if(!token) {
        return res.status(400).send({ message: "No token provided!" });
    }
    jwt.verify(token, config.secret, (err, user) => {
        if (err) {
          return res.status(401).send({ message: "Unauthorized!" });
        }
        console.log(user)
        req.name = user.name;
        next();
    });
};

isManager = (req, res, next) => {
    User.findOne({username: req.name}, (err, user) => {
        if(err) {
            res.status(500).send({ message: err });
            return;
        }
        console.log(user)
        if(user.type == "manager") {
            next();
            return;
        } else {
            res.status(400).send({ message: "Access Denied!" });
        }
    })
};

isSecurity = (req, res, next) => {
    User.findOne({username: req.name}, (err, user) => {
        if(err) {
            res.status(500).send({ message: err });
            return;
        }
        if(user.type == "security") {
            next();
            return;
        }
    })
};

const authoJWT = {
    verifyToken,
    isManager,
    isSecurity
};

module.exports = authoJWT;