let mqtt = require("mqtt");
require("dotenv").config();
let clientTopics = {
  //Input
  button: "thanhngodinh/feeds/button",
  magnetic: "thanhngodinh/feeds/magnetic",
  infrared: "thanhngodinh/feeds/infrared",
  //Output
  servo: "thanhngodinh/feeds/servo",
  led: "thanhngodinh/feeds/led",
  speaker: "thanhngodinh/feeds/speaker",
};

let BBCTopics = {
  //Input
  //button: "CSE_BBC/feeds/bk-iot-button",
  //magnetic: "CSE_BBC/feeds/bk-iot-magnetic",
  button: "thanhngo/feeds/bk-iot-button",
  //infrared: "CSE_BBC1/feeds/bk-iot-infrared",
  //servo: "CSE_BBC1/feeds/bk-iot-servo",
  magnetic: "thanhngo/feeds/bk-iot-magnetic",
  infrared: "thanhngo/feeds/bk-iot-infrared",
  servo: "thanhngo/feeds/bk-iot-servo",

  //led: "CSE_BBC/feeds/bk-iot-led",
  //speaker: "CSE_BBC/feeds/bk-iot-speaker",
  led: "thanhngo/feeds/bk-iot-led",
  speaker: "thanhngo/feeds/bk-iot-speaker",
};

let settings = {
  username: "thanhngodinh",
  key: process.env.KEY,
  clientTopics: [
    clientTopics.button,
    clientTopics.magnetic,
    clientTopics.infrared,
    clientTopics.servo,
    clientTopics.led,
    clientTopics.speaker,
  ],
  BBC: "thanhngo",
  keyBBC: process.env.KEY_BBC,
  BBCTopics: [
    BBCTopics.button,
    BBCTopics.magnetic,
    BBCTopics.led,
    BBCTopics.speaker,
  ],
  BBC1: "thanhngo",
  keyBBC1: process.env.KEY_BBC1,
  BBC1Topics: [BBCTopics.infrared, BBCTopics.servo],
};

function Data(id, name, data, unit) {
  this.id = id;
  this.name = name;
  this.data = data;
  this.unit = unit;
}

//connect Adafruit
client = mqtt.connect({
  host: "io.adafruit.com",
  port: 1883,
  username: settings.username,
  password: settings.key,
});

BBC = mqtt.connect({
  host: "io.adafruit.com",
  port: 1883,
  username: settings.BBC,
  password: settings.keyBBC,
});

BBC1 = mqtt.connect({
  host: "io.adafruit.com",
  port: 1883,
  username: settings.BBC1,
  password: settings.keyBBC1,
});

//pub
function pub(topic, data) {
  var inputData;
  switch (topic) {
    case "button":
      inputData = new Data("4", "BUTTON", data, "");
      BBC.publish(BBCTopics.button, JSON.stringify(inputData));
      break;
    case "magnetic":
      inputData = new Data("8", "MAGNETIC", data, "");
      BBC.publish(BBCTopics.magnetic, JSON.stringify(inputData));
      break;
    case "infrared":
      inputData = new Data("16", "INFRARED", data, "");
      BBC1.publish(BBCTopics.infrared, JSON.stringify(inputData));
      break;
    case "servo":
      inputData = new Data("17", "SERVO", data, "degree");
      BBC1.publish(BBCTopics.servo, JSON.stringify(inputData));
      break;
    case "led":
      inputData = new Data("1", "LED", data, "");
      BBC.publish(BBCTopics.led, JSON.stringify(inputData));
      break;
    case "speaker":
      inputData = new Data("2", "SPEAKER", data, "");
      BBC.publish(BBCTopics.speaker, JSON.stringify(inputData));
      break;
  }
}

module.exports = pub;

// client.on('connect', function () {
//     inputData = new Data('1', 'BUTTON', '0', '');
//     client.publish(clientTopics.button, JSON.stringify(inputData));
// });

// BBC.on('connect', function () {
//     pub('magnetic', '1');
// });
