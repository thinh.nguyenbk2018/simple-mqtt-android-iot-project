let mqtt = require("mqtt");
require("dotenv").config();
let clientTopics = {
  //Input
  button: "thanhngodinh/feeds/button",
  magnetic: "thanhngodinh/feeds/magnetic",
  infrared: "thanhngodinh/feeds/infrared",
  //Output
  servo: "thanhngodinh/feeds/servo",
  led: "thanhngodinh/feeds/led",
  speaker: "thanhngodinh/feeds/speaker",
};

let BBCTopics = {
  //Input
  //button: "CSE_BBC/feeds/bk-iot-button",
  //magnetic: "CSE_BBC/feeds/bk-iot-magnetic",
  button: "thanhngo/feeds/bk-iot-button",
  //infrared: "CSE_BBC1/feeds/bk-iot-infrared",
  //servo: "CSE_BBC1/feeds/bk-iot-servo",
  magnetic: "thanhngo/feeds/bk-iot-magnetic",
  infrared: "thanhngo/feeds/bk-iot-infrared",
  servo: "thanhngo/feeds/bk-iot-servo",

  //led: "CSE_BBC/feeds/bk-iot-led",
  //speaker: "CSE_BBC/feeds/bk-iot-speaker",
  led: "thanhngo/feeds/bk-iot-led",
  speaker: "thanhngo/feeds/bk-iot-speaker",
};
console.log(process.env.KEY);
console.log(process.env.KEY_BBC);
console.log(process.env.KEY_BBC1);
let settings = {
  username: "thanhngodinh",
  key: process.env.KEY,
  clientTopics: [
    clientTopics.button,
    clientTopics.magnetic,
    clientTopics.infrared,
    clientTopics.servo,
    clientTopics.led,
    clientTopics.speaker,
  ],

  BBC: "thanhngo",
  keyBBC: process.env.KEY_BBC,
  BBCTopics: [
    BBCTopics.button,
    BBCTopics.magnetic,
    BBCTopics.led,
    BBCTopics.speaker,
  ],

  BBC1: "thanhngo",
  keyBBC1: process.env.KEY_BBC1,
  BBC1Topics: [BBCTopics.infrared, BBCTopics.servo],
};

function Data(id, name, data, unit) {
  this.id = id;
  this.name = name;
  this.data = data;
  this.unit = unit;
}

//connect MongoDB
const { MongoClient } = require("mongodb");
const url = "mongodb://localhost:27017";
const clientMongo = new MongoClient(url, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
clientMongo.connect();

//connect Adafruit
client = mqtt.connect({
  host: "io.adafruit.com",
  port: 1883,
  username: settings.username,
  password: settings.key,
});

BBC = mqtt.connect({
  host: "io.adafruit.com",
  port: 1883,
  username: settings.BBC,
  password: settings.keyBBC,
});

BBC1 = mqtt.connect({
  host: "io.adafruit.com",
  port: 1883,
  username: settings.BBC1,
  password: settings.keyBBC1,
});

//subscribe topic
client.on("connect", () => {
  for (topic of settings.clientTopics) {
    client.subscribe(topic);
    console.log("Subscribed topic " + topic);
  }
});

BBC.on("connect", () => {
  for (topic of settings.BBCTopics) {
    BBC.subscribe(topic, (err) => {
      console.log(err);
    });
    console.log("Subscribed topic " + topic);
  }
});

BBC1.on("connect", () => {
  for (topic of settings.BBC1Topics) {
    BBC1.subscribe(topic);
    console.log("Subscribed topic " + topic);
  }
});

//check data
function check(msg) {
  if (
    msg.name == "BUTTON" ||
    msg.name == "MAGNETIC" ||
    msg.name == "INFRARED"
  ) {
    if (msg.data == "0" || msg.data == "1") {
      return true;
    }
  }
  if (msg.name == "LED") {
    if (msg.data == "0" || msg.data == "1" || msg.data == "2") {
      return true;
    }
  }
  if (msg.name == "SPEAKER") {
    if (parseInt(msg.data) >= 0 && parseInt(msg.data) <= 1023) {
      return true;
    }
  }
  if (msg.name == "SERVO") {
    if (parseInt(msg.data) >= 0 && parseInt(msg.data) <= 180) {
      return true;
    }
  }
  return false;
}

//pub
function pub(topic, data) {
  var inputData;
  switch (topic) {
    case "button":
      inputData = new Data("4", "BUTTON", data, "");
      BBC.publish(BBCTopics.button, JSON.stringify(inputData));
      break;
    case "magnetic":
      inputData = new Data("8", "MAGNETIC", data, "");
      BBC.publish(BBCTopics.magnetic, JSON.stringify(inputData));
      break;
    case "infrared":
      inputData = new Data("16", "INFRARED", data, "");
      BBC1.publish(BBCTopics.infrared, JSON.stringify(inputData));
      break;
    case "servo":
      inputData = new Data("17", "SERVO", data, "degree");
      BBC1.publish(BBCTopics.servo, JSON.stringify(inputData));
      break;
    case "led":
      inputData = new Data("1", "LED", data, "");
      BBC.publish(BBCTopics.led, JSON.stringify(inputData));
      break;
    case "speaker":
      inputData = new Data("2", "SPEAKER", data, "");
      BBC.publish(BBCTopics.speaker, JSON.stringify(inputData));
      break;
  }
}

let security = false;
function logic(msg) {
  var doorId = "8";
  //Check button for change security state
  if (msg.name == "BUTTON" && msg.data == "1") {
    security = !security; //Change security
    //Change LED color
    color = security ? "1" : "2";
    pub("led", color);
  }
  //Change security according to led
  if (msg.name == "LED" && msg.data == "1") {
    security = true;
    console.log("security: " + security);
    //Close all door while change to high security
    if (security == true) {
      pub("servo", 0);
    }
  } else if (msg.name == "LED" && msg.data == "2") {
    security = false;
    console.log("security: " + security);
  }

  if (security == true) {
    // High security
    //IR sensor detects theft
    if (msg.name == "INFRARED" && msg.data == "1") {
      //Save to Intrusion DB
      let cltIntrusion = clientMongo.db("MQTT").collection("intrusion");
      cltIntrusion.insertOne(
        {
          time: new Date().setHours(00, 00, 00),
          doorID: doorId,
        },
        (err, result) => {
          if (err) throw err;
          console.log("Data is inserted to Intrusion");
        }
      );
      //Request for speaker ring
      pub("speaker", "1023");
    }
    //IR sensor no longer detects theft
    else if (msg.name == "INFRARED" && msg.data == "0") {
      //Request for speaker stop ring
      pub("speaker", "0");
    }
  } else if (security == false) {
    //Low security
    //IR sensor detects client
    if (msg.name == "INFRARED" && msg.data == "1") {
      pub("servo", "90");
    }
    //IR sensor no longer detects client, request to close door
    else if (msg.name == "INFRARED" && msg.data == "0") {
      pub("servo", "0");
    }
    //switch
    if (msg.name == "MAGNETIC" && msg.data == "0") {
      //open door
      //insert entrance
      let cltEntrance = clientMongo.db("MQTT").collection("entrance");
      cltEntrance.insertOne(
        {
          time: new Date().setHours(00, 00, 00),
          doorID: doorId,
        },
        (err, result) => {
          if (err) throw err;
          console.log("Data is inserted to Entrance");
        }
      );
      //update door status
      let cltDoor = clientMongo.db("MQTT").collection("door");
      cltDoor.updateOne(
        { id: doorId },
        { $set: { state: "0" } },
        (err, result) => {
          if (err) throw err;
          console.log("Data is updated to Door (State: 0)");
        }
      );
    } else if (msg.name == "MAGNETIC" && msg.data == "1") {
      //close door
      //update door status
      let cltDoor = clientMongo.db("MQTT").collection("door");
      cltDoor.updateOne(
        { id: doorId },
        { $set: { state: "1" } },
        (err, result) => {
          if (err) throw err;
          console.log("Data is updated to Door (State: 1)");
        }
      );
    }
  }
}
//Control door by servo
client.on("message", function (topic, message) {
  msg = JSON.parse(message.toString());
  controlDoor(msg);
});
function controlDoor(msg) {
  if (msg.name == "SERVO" && msg.data == "90") {
    //open door
    pub("magnetic", "0");
  } else if (msg.name == "SERVO" && msg.data == "0") {
    //close door
    pub("magnetic", "1");
  }
}
//Control door by servo
// client.on('message', function (topic, message) {
//    msg = JSON.parse(message.toString());
//    controlDoor(msg);
// })
// function controlDoor(msg){
//    if (msg.name == 'SERVO' && msg.data == '90') { //open door
//        pub('magnetic', '0');
//    }
//    else if (msg.name == 'SERVO' && msg.data == '90') { //close door
//        pub('magnetic', '1');
//    }
// }

//Read message
BBC.on("message", function (topic, message) {
  var tp = topic.toString();
  var msg = message.toString();
  switch (tp) {
    case BBCTopics.button:
      client.publish(clientTopics.button, msg);
      break;
    case BBCTopics.magnetic:
      client.publish(clientTopics.magnetic, msg);
      break;
    case BBCTopics.led:
      client.publish(clientTopics.led, msg);
      break;
    case BBCTopics.speaker:
      client.publish(clientTopics.speaker, msg);
      break;
  }
});

BBC1.on("message", function (topic, message) {
  var tp = topic.toString();
  var msg = message.toString();
  switch (tp) {
    case BBCTopics.infrared:
      client.publish(clientTopics.infrared, msg);
      break;
    case BBCTopics.servo:
      client.publish(clientTopics.servo, msg);
      break;
  }
});

client.on("message", function (topic, message) {
  msg = JSON.parse(message.toString());
  console.log(msg);
  if (check(msg)) {
    logic(msg);
  } else {
    console.log("Invalid input!");
  }
});
